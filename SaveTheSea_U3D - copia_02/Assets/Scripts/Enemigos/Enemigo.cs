﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public ControladorJuego controlJuego;
    public float velocidad;
    public int numEnemigo;
    public AudioClip audioChocarSuelo;
    AudioSource audioCogerLata;

    //dos objetos/componente a enalazar
    //private ControladorJuego ctroljuego;
    GameObject jugador;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Jugador");
        audioCogerLata = GameObject.Find("Captura_de_lata").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;
        this.GetComponent<Transform>().position += movAbajo;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this.enabled)
        {
            Debug.Log("Enemigo colisionado con " + collision.gameObject.name);
            if (collision.gameObject.name.ToLower().Contains("fondo"))
            {
                // Deshabilitar este mismo componente Enemigo.cs
                this.enabled = false;
                // Detener la animación
                this.GetComponent<Animator>().speed = 0;
                controlJuego.RestarVida(this.numEnemigo);
                AudioSource.PlayClipAtPoint(audioChocarSuelo, Vector3.zero);
            }
            // Que cuando choque con jugador, que destruya el enemigo (buscar cómo destruir un game object)
            if (collision.gameObject.name.ToLower().Contains("cuerpo"))
            {
                audioCogerLata.Play();
                Destroy(this.gameObject);
                controlJuego.SumarPuntos(100, this.numEnemigo);
            }
        }
    }

}
