﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Comentario de pruba
namespace GermanSTS
{
    // Clase ControladorEscenas
    public class ControladorEscenas : MonoBehaviour
    {
        [Header("Propiedades ctrl escenas:")]
        public string nombreEscena;

        private void Start()
        {
            if (nombreEscena != "")
            {
                print("Arrancando escena " + nombreEscena);
                this.CargarEscena(nombreEscena);
            }
            AsignarCerrarAlBotonSalir();
            /*
            GameObject botonsalir = GameObject.Find("Button_SALIR_Si");
            botonsalir.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(this.CerrarAplicacion);*/
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                CerrarAplicacion();
            }
        }
        public void CargarEscena(string escena)
        {
            SceneManager.LoadScene(escena);
        }

        public void CerrarAplicacion()
        {
            Application.Quit();
        }
        void AsignarCerrarAlBotonSalir()
        {
            GameObject objCanvas = GameObject.Find("Canvas");

            if (objCanvas != null)
            {
                Transform canvas = GameObject.Find("Canvas").GetComponent<Transform>();
                Transform button_SALIR_Si = null;
                FindByNameInactives(canvas, "Button_SALIR_Si", ref button_SALIR_Si);
                if (button_SALIR_Si != null)
                {
                    button_SALIR_Si.GetComponent<UnityEngine.UI.Button>()
                    .onClick.AddListener(this.CerrarAplicacion);
                }
            }
        }
        void FindByNameInactives(Transform raiz, string nombre, ref Transform objetoEncontrado)
        {
            for (int i = 0; i < raiz.childCount; i++)
            {
                Transform objTransf = raiz.GetChild(i);
                if (objTransf.name == nombre)
                {
                    objetoEncontrado = objTransf;
                    return;
                }
                else
                {
                    FindByNameInactives(objTransf, nombre, ref objetoEncontrado);
                }
            }
        }
    }
}
