﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    public float velocidad = 1f;

    /*private void Update()
        {
        Vector3 posicion = GetComponent<Transform>().position;
        posicion.x = velocidad * Time.time;
        transform.position = posicion;
         }*/

    //si direccion vale
    //+1 mueve derecha
    //-1 mueve izqda
    //0 no se mueve
    public void Mover(int direccion)
        {
            Vector3 posicion = GetComponent<Transform>().position;
            posicion.x = posicion.x + direccion * velocidad * Time.deltaTime;
            transform.position = posicion;
            //Debug.Log("Time = " + Time.time * 1000 +  ",dt = " + Time.deltaTime * 1000 + ", X = " +posicion.x);
        }

        //EmularRetrasoCPU_GPU();

    private void EmularRetrasoCPU_GPU()
        {
        int vueltas = Random.Range(100000, 30000000);
        for(int v = 0; v < vueltas; v++)
            {
            v = v * 1 / 1; //nada solo retrasamos
            }
        }
}
