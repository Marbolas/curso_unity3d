﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{ 
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            {
            //invocamos al movimiento
            GetComponent<MovimientoJugador>().Mover(1);
            //Getcomponent<Animator>.setbool("corriendo")
            }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //invocamos al movimiento
            GetComponent<MovimientoJugador>().Mover(-1);
            //Getcomponent<Animator>.setbool("corriendo")
        }
    }
}
