﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColocarNumUI2: MonoBehaviour
{
    public List<Text> numeros;
    public List<Text> numeros2;

    List<int> listaNum;
    List<int> copialistaNum;
    List<int> listaFinal;
    List<int> listaOrdenadaBurbuja;

    int menorDeTodos;
    int indiceMenor;
    int auxIndiceMenor;
    public UnityEngine.Vector2 posInicial;

    // Start is called before the first frame update
    void Start()
    {
        listaNum = new List<int>();
        listaFinal = new List<int>();
        listaOrdenadaBurbuja = new List<int>();

        foreach (Text txtnum in numeros)
        {
            int num = Int32.Parse(txtnum.text);
            listaNum.Add(num);
           // copialistaNum.Add(num);
        }

        copialistaNum = new List<int>(listaNum);                            //Creamos una listaauxliar porque la primera desaparece al ordenarla
        for(int i = 0; i < copialistaNum.Count; i++)
        {
            Debug.Log("Elemento " + i + " = " + copialistaNum[i]);
        }

        menorDeTodos = OrdenarComoHumano.BuscarMenor(listaNum);
        listaFinal = OrdenarComoHumano.Ordenar(listaNum);
        ConverListordEnTextNumeros(listaFinal, numeros2);
        //listaOrdenadaBurbuja = OrdenarComoHumano.OrdenarBurbuja(copialistaNum);
        //ColocarSeguidos();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ConverListordEnTextNumeros(List<int> ordenada, List<Text> numeros2)     //pegar valores de la lista de enteros en lista de texto
    {
        for (int i = 0; i < ordenada.Count; i++)
        {
            numeros2[i].text = ordenada[i].ToString();
            Debug.Log("Elemento de texto nuevo es: " + numeros2[i]);
        }
    }


    public void ColocarSeguidos()
    {
        indiceMenor = OrdenarComoHumano.BuscarMenor2(listaNum);     //Devuelve el indice de la posesión del menor

        this.posInicial = this.numeros[0].transform.GetComponent<RectTransform>().localPosition;    //Ponemos el numero mas bajo en la posición de el 1º
        this.numeros[indiceMenor].rectTransform.localPosition = this.posInicial;

        auxIndiceMenor = indiceMenor;   //variable auxiliar para no perder el indice de referencia.

            for (int i = 1; i < this.numeros.Count; i++)
            {
                indiceMenor = OrdenarComoHumano.BuscarMenor2(listaNum);

                float nuevaPosX = this.numeros[auxIndiceMenor].rectTransform.localPosition.x +
                    (this.numeros[auxIndiceMenor].rectTransform.rect.width / 2) +
                    (this.numeros[indiceMenor].rectTransform.rect.width / 2);
                float nuevaPosY = this.posInicial.y;

                Vector2 nuevaPos = new Vector2(nuevaPosX, nuevaPosY);
                this.numeros[indiceMenor].rectTransform.localPosition = nuevaPos;
                
                 auxIndiceMenor = indiceMenor;
            }
    }
}
