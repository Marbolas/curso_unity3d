﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrdenarComoHumano
{
	public static void Main()
	{
		List<int> numeros = new List<int>();

		numeros.Add(4);
		numeros.Add(5);
		numeros.Add(3);
		numeros.Add(2);
		numeros.Add(1);
		
		MostrarLista(numeros);		
		Ordenar(numeros);			//poner numeros =  delante
		MostrarLista(numeros);
	}

	static void MostrarLista(List<int> lista)
    {
		Debug.Log("La lista es: ");
		for(int i=0; i< lista.Count; i++)
        {
			Debug.Log(lista[i] + ",");
        }
		Debug.Log(" FIN ");
	}

	public static int BuscarMenor(List<int> lista)
    {
		int menor = lista[0];
		for (int i=0; i < lista.Count; i++)
        {
			if (lista[i] < menor)
			{
				menor = lista[i];      
			}
        }
		Debug.Log("El número menor es: " + menor);
		return menor;       //podemos utilizarlo despues?
    }

	public static int BuscarMenor2(List<int> lista)		//Retorna el indice de la posicion del menor
	{
		int indice = 0;
		int menor = lista[0];

		for (int i = 0; i < lista.Count; i++)
		{
			if (lista[i] < menor)
			{
				indice = i;
			}
		}
		Debug.Log("El número menor es: " + menor);
		return indice;    //podemos utilizarlo despues?
	}

	public static List<int> Ordenar(List<int> lista)			//poner List<int> delante
    {
		int auxmenor = 0;
		List<int> listaordenados = new List<int>();

		while(0 < lista.Count)
        {
			auxmenor = BuscarMenor(lista);
			listaordenados.Add(auxmenor);
			Debug.Log("El tamaño de la lista listaordenados = " + listaordenados.Count);
			MostrarLista(listaordenados);
			lista.Remove(auxmenor);
			Debug.Log("El tamaño de la lista es: " + lista.Count);
			MostrarLista(lista);
		}
		Debug.Log("El tamaño final de la lista listaordenados es = " + listaordenados.Count);
		return listaordenados;
	}

	public static List<int> OrdenarBurbuja(List<int> lista)
	{
		int auxvar = 0;
		/*int i = 0;
		int j = 0;*/

		for(int i = 0; i < lista.Count; i++)
		{
			for(int j = i + 1; j < lista.Count; j++)
			{
				if(lista[i] > lista[j])
				{
					auxvar = lista[i];
					lista[i] = lista[j];
					lista[j] = auxvar;
				}
			}
		}
		return lista;
	}

	/*public static List<int> Desordenar(List<int> lista)
	{
		Random random = new Random()
	}*/

}
