﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColocarNumUI: MonoBehaviour
{
    public List<Text> numeros;
    List<int> listaNum;
    List<int> copialistaNum;
    List<int> listaFinal;
    List<int> listaOrdenadaBurbuja;

    int menorDeTodos;
    public UnityEngine.Vector2 posInicial;
    //const int ANCHOTEXTO = 160;                                             //Ancho de la caja de los números
    //public UnityEngine.Vector2 DIMENSIONES = new Vector2(ANCHOTEXTO,0);

    // Start is called before the first frame update
    void Start()
    {
        listaNum = new List<int>();
        listaFinal = new List<int>();
        listaOrdenadaBurbuja = new List<int>();

        foreach (Text txtnum in numeros)
        {
            int num = Int32.Parse(txtnum.text);
            listaNum.Add(num);
           // copialistaNum.Add(num);
        }

        copialistaNum = new List<int>(listaNum);
        for(int i = 0; i < copialistaNum.Count; i++)
        {
            Debug.Log("Elemento " + i + " = " + copialistaNum[i]);
        }
        menorDeTodos = OrdenarComoHumano.BuscarMenor(listaNum);
        //listaFinal = OrdenarComoHumano.Ordenar(listaNum);
        //listaOrdenadaBurbuja = OrdenarComoHumano.OrdenarBurbuja(copialistaNum);
        ColocarSeguidos();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ColocarSeguidos()
    {
        //int i = 1;

        this.posInicial = this.numeros[0].transform.GetComponent<RectTransform>().localPosition;
        for (int i = 1; i < this.numeros.Count; i++)
        {
            float nuevaPosX = this.numeros[i - 1].rectTransform.localPosition.x +
                (this.numeros[i - 1].rectTransform.rect.width / 2) +
                (this.numeros[i].rectTransform.rect.width / 2);
            float nuevaPosY = this.posInicial.y;

            Vector2 nuevaPos = new Vector2(nuevaPosX, nuevaPosY);
            this.numeros[i].rectTransform.localPosition = nuevaPos;
        }

        /*for (int i = 1; i < this.numeros.Count; i++)
        {
            float nuevaPosX = this.numeros[i - 1].rectTransform.localPosition.x +
                (this.numeros[i - 1].rectTransform.rect.width / 2);  
            float nuevaPosY = this.posInicial.y;
            Vector2 nuevaPos = new Vector2(nuevaPosX, nuevaPosY);
            this.numeros[i].rectTransform.localPosition = nuevaPos;
        }*/
    }



    /*public void ColocarSeguidos()
    {
        List<int> listaPosX = new List<int>();
        
        foreach (Text txtNum in this.numeros)
        {
            int posX = (int)txtNum.rectTransform.localPosition.x;
            listaPosX.Add(posX);
        }
        int posMenorX = OrdenarComoHumano.BuscarMenor(listaPosX);
        this.posInicial = new Vector2(posMenorX, this.numeros[0].rectTransform.rect.position.y);

        this.numeros[0].rectTransform.localPosition = this.posInicial;

        /*for (int i = 1; i < this.numeros.Count; i = i + 1)
        {
            Vector2 posSiguiente = this.posInicial + DIMENSIONES);
            int posSiguiente = posmenorX + ANCHOTEXTO;
            Vector2 vPosSig = new Vector2()posSiguiente, this.numeros[i].rectTransform.position.y);
        }

    }*/
}
