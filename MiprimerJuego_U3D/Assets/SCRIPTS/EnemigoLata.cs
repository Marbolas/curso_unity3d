﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoLata : MonoBehaviour
{
    public float velocidad; // Por defecto, cero
    GameObject jugador;


    //Start se llama la primera vez, primer frame
    private void Start()
    {
        float posInicioX = Random.Range(-10, 10);       //Damos a la lata una posición aleatoria
        this.transform.position = new Vector3(posInicioX, 12, -1);

        jugador = GameObject.Find("jugadorcaballito");    
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad        //definimos vector movimiento en base a velocidad
            * new Vector3(0, -1, 0)
            * Time.deltaTime;

        this.GetComponent<Transform>().position =       //la lata baja
            this.GetComponent<Transform>().position
            + movAbajo;

        if (this.GetComponent<Transform>().position.y < 0)      //
        {   
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, -1); //con un espacio

            if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
                && this.transform.position.x <= jugador.transform.position.x + 3.2f / 2)
            {
                GameObject.Find("Controlador-Juego")        //Se suman 10 puntos por acierto
               .GetComponent<ControladorJuego>()
               .CuandoCapturamosEnemigo();
                Destroy(this.gameObject);
            }
            else
            {
                GameObject.Find("Controlador-Juego")        //Se resta una vida
                .GetComponent<ControladorJuego>()
                .CuandoPerdemosEnemigo();
                Destroy(this.gameObject);
            }

        }
    }
}
