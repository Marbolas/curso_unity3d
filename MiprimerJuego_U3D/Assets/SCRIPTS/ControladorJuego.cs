﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ControladorJuego : MonoBehaviour
{
    public GameObject[] enemigos;

    /*public GameObject prefablata;
    public GameObject prefabbotella;*/
    public int vidas = 7;
    public int puntos = 0;
    public GameObject textovidas;
    public GameObject textopuntos;
    public int crearEnemigo = 0;   //controla que tipo de enemigo se crea
    private int puntosAnt;  //puntos del frame anterior


    // Start is called before the first frame update
    void Start()
    {
        //GameObject.Instantiate(enemigos[0]);
        instanciarEnemigo();

    }

    // Update is called once per frame
    void Update()
    {
        this.textovidas.GetComponent<Text>().text = "Vidas: " + this.vidas;
        this.textopuntos.GetComponent<Text>().text = "Puntos:" + this.puntos;
    }

    //nuevo método(acción,función,procedimientto,conjunto de instrucciones,mensajes...)
    public void CuandoCapturamosEnemigo()
    {
        if (crearEnemigo == 0)  //controlamos que enemigo crear
        {
            this.puntos = this.puntos + 10;     //asignando un nuevo valor a la puntuacìón this.puntos +=10
            instanciarEnemigo();
            crearEnemigo = 1;
        }
        else
        {
            this.puntos = this.puntos + 10;
            instanciarEnemigo();
            crearEnemigo = 0;
        }
    }

    public void CuandoPerdemosEnemigo()
    {
        this.vidas = this.vidas - 1;    //this.vidas -= 1 this.vidas--
        //GameObject.Instantiate(enemigos[1]);
        instanciarEnemigo();
    }
    public void instanciarEnemigo()
    {
        int numEnemigo = Random.Range(0,enemigos.Length);
        GameObject.Instantiate(enemigos[numEnemigo]);
    }
}
