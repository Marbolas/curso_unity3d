﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basura : MonoBehaviour
{
    public float velocidad = 40;

    // Update is called once per frame
    void Update()
    {
        Vector3 vectorMov = velocidad * Vector3.down * Time.deltaTime;

        if (this.GetComponent<Transform>().position.y == -3)        //si ha llegado al fondo
        {
            //entonces recolocamos en el fondo
            this.GetComponent<Transform>().position = new Vector3(0, -3, 0);
        }

        if (this.GetComponent<Transform>().position.y > -3)     // se mueve hacia bajo
        {
            this.GetComponent<Transform>().Translate(vectorMov);
            //float inc = 0;
            //inc = inc  1;
            //entonces recolocamos en el margen izq
            //this.GetComponent<Transform>().position = new Vector3(0, 0, 0);
        }
    }
}
