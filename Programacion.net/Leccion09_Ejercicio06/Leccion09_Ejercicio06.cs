﻿using System;
using System.Collections.Generic;

namespace Leccion09_Ejercicio06
{
    class Leccion09_Ejercicio06       /**Realizar un programa que permita cargar dos listas de 15 valores cada una. Informar con un mensaje
                                         cual de las dos listas tiene un valor acumulado mayor (mensajes "Lista 1 mayor", "Lista 2 mayor", "Listas iguales"). Tener en cuenta
                                         que puede haber dos o más estructuras repetitivas en un algoritmo. */
    {
        static void Main(string[] args)
        {
            List<float> lista = new List<float>();
            List<float> lista2 = new List<float>();
            int num = 0;
            float valor = 0;
            string linea;

            float contlista = 0;
            float contlista2 = 0;

            while(num < 5)
            {
                Console.WriteLine("introduce el elemento: " + num + " en la lista 1");
                linea = Console.ReadLine();
                valor = float.Parse(linea);
                lista.Add(valor); 

                Console.WriteLine("introduce el elemento: " + num + " en la lista 2");
                linea = Console.ReadLine();
                valor = float.Parse(linea);
                lista2.Add(valor);
                num++;
            }
            num = 0;
            while(num < 5)
            {
                contlista = contlista + lista[num];
                contlista2 = contlista2 + lista2[num];
                num++;
            }

            if(contlista > contlista2)
                Console.WriteLine("La lista 1 es mayor. " + contlista + " >  que " + contlista2);
                else if(contlista < contlista2)
                    Console.WriteLine("La lista 2 es mayor. " + contlista2 + " >  que " + contlista);
                    else
                         Console.WriteLine("La listas son iguales. " + contlista + " = " + contlista2);
            Console.ReadKey();
        }
    }
}
