﻿using System;

namespace Leccion17_Ejercicio01

{
    class Vectoresparalelos     /**Desarrollar un programa que permita cargar 5 nombres de personas y sus edades respectivas. 
                                   Luego de realizar la carga por teclado de todos los datos imprimir los nombres de las personas
                                   mayores de edad (mayores o iguales a 18 años)*/
    {
        private string[] vectorNom;
        private int[] vectorEdad;

        public void Cargar()
        {
            vectorNom = new string[5];
            vectorEdad = new int[5];

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Introduce el nombre:");
                vectorNom[i] = Console.ReadLine();
                Console.WriteLine("Introduce la edad:");
                vectorEdad[i] = Int32.Parse(Console.ReadLine());
            }
        }

        public void Imprimir()
        {
            for (int i = 0; i < 5; i++)
            {
                if (vectorEdad[i] >= 18)
                {
                    Console.WriteLine("Nombre: " + vectorNom[i]);
                    Console.WriteLine("Edad: " + vectorEdad[i]);
                }
            }
        }
        static void Main(string[] args)
        {
            Vectoresparalelos vParalelo = new Vectoresparalelos();
            vParalelo.Cargar();
            vParalelo.Imprimir();
        }
    }
}
