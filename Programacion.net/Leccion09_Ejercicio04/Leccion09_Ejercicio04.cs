﻿using System;

namespace Leccion09_Ejercicio04
{
    class Leccion09_Ejercicio04     /**Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. 
                                       (No se ingresan valores por teclado) */
    {
        static void Main(string[] args)
        {
            int n = 1;
            int numero = 11;

            while (n <= 25)
            {
                Console.Write(numero + " , ");
                numero = numero + 11;
                n++;
            }
        }
    }
}
