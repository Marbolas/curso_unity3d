﻿using System;

struct Jugador
	{
	public string nombre;
	public int edad;
	public bool haPagado;
	public float vida;		//True Ha pagado, false es free to play

	//Constructor : Funcion que seirve para inicializar los datos de una estructura del tirón.En las
	//estructuras es necesasrio inicializar todas las variables miembro (campos, propiedades atributos).

	public Jugador(string n,int edad, bool haPagado, float vida)
		{
		nombre = n;
		this.edad = edad;
		this.haPagado = haPagado;
		this.vida = vida;
		}

	public void Mostrar()
		{
		Console.WriteLine("Persona: " + this.nombre + " Edad: " + this.edad
		+ (haPagado ? " VIP " : " Freee to play ") + "Vida: " + vida);
		}
}

struct Enemigo
{
	public string nombre;
	public float ataque;

	public Enemigo(string n, float ataque)
	{
		nombre = n;
		this.ataque = ataque;
	}

	public void MostrarEnemigo()
	{
		Console.WriteLine("Nombre : " + this.nombre + " Ataque: " + ataque);
	}

	public void Ataque(ref Jugador jug)		//Recibimos la referencia del jugador
	{
		jug.vida = jug.vida - this.ataque;
		//Console.WriteLine("Nombre : " + jug.nombre + " Vida = " + jug.vida);
		jug.Mostrar();
	}

	public void Ataque(ref float vida)     //Recibimos la referencia del jugador
	{
		vida = vida - this.ataque;
	}

	public void AtaqueVarios(Jugador[]jugadores)
    {
		for(int i =0; i < jugadores.Length; i++)
        {
			jugadores[i].vida = jugadores[i].vida - this.ataque;
			jugadores[i].Mostrar();
		}
	}
}

public class Estructuras
{
	static public void Main()
	{
		Jugador jug = new Jugador("Fulano", 30, false,100f);
		jug.Mostrar();
		//Mostrar(jug.nombre, jug.edad, jug.haPagado);

		Jugador jug2 = new Jugador("Fulanita", 32, true,100f);
		jug2.Mostrar();
		//Mostrar(jug2.nombre, jug2.edad, jug2.haPagado);

		Jugador jug3 = new Jugador("Fulanón", 35, false,100f); ;
		jug3.Mostrar();
		//Mostrar(jug3.nombre, jug3.edad, jug3.haPagado);

		Enemigo enem = new Enemigo("German", 10f);
		Enemigo enem2 = new Enemigo("Nacho", 20f);
		enem.MostrarEnemigo();
		enem2.MostrarEnemigo();
		//MostrarEnemigo(enem.nombre, enem.ataque);

		enem.Ataque(ref jug);   //Pasamos la referencia del jugador		70 de vida

		Jugador[] todoslosjug = new Jugador[2];
		Jugador[0] = jug;
		Jugador[1] = jug2;

		enem2.AtaqueVarios(ref jugador);
		enem2.AtaqueVarios(new Jugador[] { jug, jug2, jug3 });  //pasar un array	80 de vida
		enem2.AtaqueVarios(new Jugador[] { jug, jug2, jug3 });  //verificar que actualiza los daños a vida

		/*enem2.Ataque(ref jug);		
		enem2.Ataque(ref jug2);
		enem2.Ataque(ref jug3);*/

		jug.Mostrar();
		jug2.Mostrar();
		jug3.Mostrar();

		//1.Crear otro enemigo.
		//2.Añadir otro campo vida de tipo float al jugador.
		//3.Hacer un metodo ataque en enemigo que reciba un jugador como parametro , y que le quite vida.
		//4.Usarlos:Enemigo 1 ataca al jugador 1 y enemigo 2 a todos los jugadores
	}
}
