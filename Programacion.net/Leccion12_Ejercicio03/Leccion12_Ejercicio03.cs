﻿using System;
using System.Collections.Generic;

//    

namespace Leccion12_Ejercicio3
{
    class SepararPorNum
    {
        // Ahora hacemos nombreEn.. como variable GLOBAL:
        // Significa que SÓLO hay UNA por Aplicación.
        // En C# se les llama variables estáticas.
        static string[] nombresEnemigos =
        {
            "Lata 1",           // Objetos de la escena precargados
            "Lata 22 (Clone)", // Objetos de la escena instanciados por código, osea, dinámicos
            "   Lata 2 (Clone)   ",
            "Lata 2 (CLONE)",
            "   Lata 2   " ,
            "Basura roja 11",
            "Basura roja 1 (Clone)",
            "Basura 32",
            "Basura 3"
        };

        // Ahora lo que hace es leer el array y meter en dos listas los strings que 
        // contienen Clone  y los que no, usando  Contains()
        // Ejercicio 1: Similar, meter en dos listas los de 1 y los de 2 usando IndexOf()
        // Ejercicio 2: Que los que sean de TIPO 2, los meta en otra lista
        // Ejercicio 3: Que los que sean de TIPO 3, los meta en una tercera lista
        // Ejercicio 4: Hacer una función para ello, bool SaberSiEsTipo(String num)
        //              Que las 3 formas usen la esta MISMA FUNCION. Es decir, no repetir el código 3 veces, sólo una
        static void Main(string[] args)
        {   
            SepararPorNumeros();

            //Console.WriteLine(String.IsNullOrWhiteSpace("Hola que pasa")); Busca espacio
        }
        
        static void SepararPorNumeros()
        {
            List<string> obj_1 = new List<string>();
            List<string> obj_2 = new List<string>();
            List<string> obj_3 = new List<string>();
            List<string> obj_4 = new List<string>();
          
            string enemigoActual = " ";

            for (int i = 0; i < nombresEnemigos.Length; i++)
            {
                enemigoActual = nombresEnemigos[i];

                if (SaberSiEsTipo(enemigoActual, "1"))
                    {
                    obj_1.Add(enemigoActual);
                }
                else if
                    (SaberSiEsTipo(enemigoActual, "2"))
                    {
                    obj_2.Add(enemigoActual);
                }
                else if
                    (SaberSiEsTipo(enemigoActual, "3"))
                    {
                    obj_3.Add(enemigoActual);
                }
                else
                {
                    obj_4.Add(enemigoActual);
                    Console.WriteLine("NÚMERO DIFERENTE");
                }
                    

            }
            Imprimir("1", obj_1);
            Imprimir("2", obj_2);
            Imprimir("3", obj_3);
            Imprimir("DIFERENTE", obj_4);
        }

        static bool SaberSiEsTipo(string enemigoActual, string num)
        { 
            if (enemigoActual.IndexOf(num) >= 0)        // Si contiene un UNO, puede ser...
            { 
                int posChar_1 = enemigoActual.IndexOf(num);     // Cojemos la posición del 1
                if (enemigoActual.Substring(posChar_1 - 1, 1) == " ")       // Si el anterior al 1 es un espacio...
                { 
                    if (posChar_1 == enemigoActual.Length - 1)      // Entonces si es el último caracter
                    {  
                        return true;                                 // Bien! Es del tipo 1
                    }
                    else // Pero puede ser que no sea el último 
                    {
                        string sigChar = enemigoActual.Substring(posChar_1 + 1, 1);         // Entonces cogemos el siguiente caracter
                        //int numDelChar;
                        if (!int.TryParse(sigChar, out int numDelChar))             //Y comprobamos que NO sea un número
                        {
                            return true;                    // Si consigue hacer el parseo, es número. Al negarlo, es que NO ES NÚMERO: 
                        }
                    }
                }
            }
            return false;
        }

        static void Imprimir(string num, List<string> lista)
        {
            Console.WriteLine("TIPO " + num + " = " + lista.Count);
            foreach (string enemAc in lista)
            {
                Console.WriteLine("Tipo" + num + ": " + enemAc);
            }
        }
    }
}
