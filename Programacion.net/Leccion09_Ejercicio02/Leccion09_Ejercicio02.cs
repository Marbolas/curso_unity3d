﻿using System;

namespace Leccion09_Ejercicio02
{
    class Leccion09_Ejercicio02   /**Se ingresan un conjunto de n alturas de personas por teclado.Mostrar la altura promedio de las personas.*/
    {
        static void Main(string[] args)
        {

            float altura = 0;
            int n = 1;
            int personas = 0;
            float totalalturas = 0;
            float promedio = 0;
            string linea;

            Console.WriteLine("Introduce el número de personas: ");
            linea = Console.ReadLine();
            personas = int.Parse(linea);

            while (n <= personas)
            {
                Console.WriteLine("Introduce la altura de la persona: " + n);
                linea = Console.ReadLine();
                altura = float.Parse(linea);

                totalalturas = totalalturas + altura;
                n++;
            }
            promedio = totalalturas / personas;
            Console.WriteLine("La altura media de las personas es: " + promedio + "cm");
            Console.ReadKey();
        }
    }
}
