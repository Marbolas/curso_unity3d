using System;

public class Funciones
{
	public static void Main()
	{
			//para usar una función ,ponemos el nombre y entre paréntesis losargumentos (param)
		double y;
		string valorUsuario; 
		double valUsu;
		Console.WriteLine("Introduzca un valor de X: ");
		valorUsuario = Console.ReadLine();
		valUsu = Double.Parse(valorUsuario);
		y = Funcionlineal2x3(valUsu);
		Console.WriteLine("Resultado  2 * " + valUsu + " + 3 = " + y );
		
		//z = Funcionlineal2(valUsu);
		Console.WriteLine( "resultado de X^2 + 1 = " + Funcionlineal2((float)valUsu) );
		
		
	}
	//forma de una función estática
	//<Modificador acceso> static <tipo de dato result> <nombre de la función> (<tipo> param1,<tipo>) param2...)
	//Y luego entre llaves el cuerpo de la función
	//Con return podemos devolver un valor
	private static double Funcionlineal2x3(double x)
	{
		return x * 2 + 3;
	}
	static float Funcionlineal2(float x)	//2 elevado a X + 1
	{
		float resultado = x * x + 1;
		return resultado;
	}
}