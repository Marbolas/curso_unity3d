﻿using System;
using System.Collections.Generic;

public class ordenarComoHumano
{
	public static void Main()
	{
		List<int> numeros = new List<int>();

		numeros.Add(4);
		numeros.Add(5);
		numeros.Add(3);
		numeros.Add(2);
		numeros.Add(1);
		
		MostrarLista(numeros);		
		numeros = Ordenar(numeros);
		MostrarLista(numeros);
	}

	static void MostrarLista(List<int> lista)
    {
		Console.WriteLine("La lista es: ");
		for(int i=0; i< lista.Count; i++)
        {
			Console.WriteLine(lista[i] + ",");
        }
		Console.WriteLine(" FIN ");
	}

	public static int BuscarMenor(List<int> lista)
    {
		int menor = lista[0];
		for (int i=0; i < lista.Count; i++)
        {
			if (lista[i] < menor)
			{
				menor = lista[i];      
			}
        }
		Console.WriteLine("El número menor es: " + menor);
		return menor;       //podemos utilizarlo despues?
    }

	public static List<int> Ordenar(List<int> lista)
    {
		int auxmenor = 0;
		List<int> listaordenados = new List<int>();

		while(0 < lista.Count)
        {
			auxmenor = BuscarMenor(lista);
			listaordenados.Add(auxmenor);
			Console.WriteLine("El tamaño de la lista listaordenados = " + listaordenados.Count);
			MostrarLista(listaordenados);
			lista.Remove(auxmenor);
			Console.WriteLine("El tamaño de la lista es: " + lista.Count);
			MostrarLista(lista);
		}
		Console.WriteLine("El tamaño final de la lista listaordenados es = " + listaordenados.Count);
		return listaordenados;
	}
}
