﻿using System;

namespace Leccion08_Ejercicios06
{
    class Leccion08_Ejercicio06   /**De un operario se conoce su sueldo y los años de antigüedad. Se pide confeccionar un programa 
                                     que lea los datos de entrada e informe:
                                     a) Si el sueldo es inferior a 500 y su antigüedad es igual o superior a 10 años, otorgarle un
                                        aumento del 20 %, mostrar el sueldo a pagar.
                                     b)Si el sueldo es inferior a 500 pero su antigüedad es menor a 10 años, otorgarle un aumento
                                       de 5 %.
                                     c) Si el sueldo es mayor o igual a 500 mostrar el sueldo en pantalla sin cambios. */
    {
        static void Main(string[] args)
        {
            int sueldo, antiguedad;
            string linea;

            Console.WriteLine("Introduce el sueldo");
            linea = Console.ReadLine();
            sueldo = int.Parse(linea);
            Console.WriteLine("Introduce la antiguedad");
            linea = Console.ReadLine();
            antiguedad = int.Parse(linea);

            if (sueldo < 500 && antiguedad >= 10)
            {
                sueldo = sueldo + ((20 * sueldo) / 100);
                Console.WriteLine(" El sueldo es: " + sueldo);
            }
            else if (sueldo < 500 && antiguedad < 10)
            {
                sueldo = sueldo + ((5 * sueldo) / 100);
                Console.WriteLine(" El sueldo es: " + sueldo);
            }
            else 
            {
                if (sueldo > 500)
                {
                    Console.WriteLine(" El sueldo es: " + sueldo);
                }
            }
        }
    }
}
