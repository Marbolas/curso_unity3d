﻿using System;

namespace Leccion05_Ejercicio03
{
    class Program   /**Se ingresa por teclado un número positivo de uno o dos dígitos(1..99) 
                    mostrar un mensaje indicando si el número tiene uno o dos dígitos.
                    (Tener en cuenta que condición debe cumplirse para tener dos dígitos, un número entero)*/
    {
        static void Main(string[] args)
        {
            int num;
            string linea;
            Console.WriteLine("Introduzca un número del 1 al 99");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            if (num > 9)
                Console.WriteLine("el número tiene dos dígitos");
            else
                Console.WriteLine("El número tiene 1 dígito");
            Console.ReadKey();
        }
    }
}
