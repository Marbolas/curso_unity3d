﻿using System;

namespace Leccion16_Ejercicio01
{
    class SumaVector    /**Desarrollar un programa que permita ingresar un vector de n elementos, ingresar n por teclado.
                           Luego imprimir la suma de todos sus elementos */
    {
        private int[] vector;

        public void Cargar()
        {
            Console.WriteLine("introduce el tamaño del vector:");
            int tamaño = int.Parse(Console.ReadLine());
            vector = new int[tamaño];

            for (int i = 0; i < vector.Length; i++)
            {
                Console.WriteLine("Introduce un elemento del vector:");
                vector[i] = int.Parse(Console.ReadLine());
            }
        }

        public void SumarVector()
        {
            int suma = 0;
            for (int i = 0; i < vector.Length; i++)
            {
                suma = suma + vector[i];
            }
            Console.WriteLine("La suma es: " + suma);
        }
        static void Main(string[] args)
        {
            SumaVector sVect = new SumaVector();
            sVect.Cargar();
            sVect.SumarVector();
        }
    }
}
