using System;

/*Estos son los tipos primitivos de C# (igual que en otros lenguajes de programación

*/

public class TiposDeDatos
{
	public static void Main()
	{
		byte unByte = 255;
		char unCaracter = 'A';
		int numEntero = 1000;	//
		
		Console.WriteLine("Byte: " + unByte + " char: " + unCaracter);
		Console.WriteLine("Un char ocupa " + sizeof(char) + " bytes");
		Console.WriteLine("El entero vale " + numEntero);
		Console.WriteLine("Y ocupa " + sizeof(int) + " bytes") ;
		numEntero = 1000000000; //mil millones
		Console.WriteLine("Ahora el entero vale " + numEntero);
		
		long enteroLargo = 5000000000;
		Console.WriteLine("Entero largo vale " + enteroLargo);
		//Tipos de decimales:float (4 bytes) y double (8 bytes)
		//Precisión es de:   7-8 cifras.		   16-16 cifras en Total
		float numDecimal = 1.23456789f;
		Console.WriteLine("Numero decimal vale " + numDecimal);
		//para mas precision: double
		double numDoubleprecision = 12345.67890123456789;
		Console.WriteLine("Num decimal doble vale " + numDoubleprecision);
		
		//Para guardar Si/No, Verdad/Falso,Cero/uno
		bool variableBooleana = true;
		Console.WriteLine("VariableBooleana vale " + variableBooleana);
		//podemos guardar comparaciones,condiciones, etc..
		variableBooleana = numDecimal > 1000;
		Console.WriteLine("VariableBooleana ahora es " + variableBooleana);
		variableBooleana = numDecimal <= 1000;
		Console.WriteLine("VariableBooleana ahora es " + variableBooleana);
		
		string cadenadetexto ="Pues eso, una cadena de texto";
		Console.WriteLine(cadenadetexto);
		Console.WriteLine(cadenadetexto + " que " + "permite concatenación");
		//Lo que no permite son conversiones inválidas:
		string otroTxt = "" + numEntero;
		
	}
}
	