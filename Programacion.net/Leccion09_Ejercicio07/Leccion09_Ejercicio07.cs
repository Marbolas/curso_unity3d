﻿using System;

namespace Leccion09_Ejercicio07
{
    class Leccion09_Ejercicio07     /**Desarrollar un programa que permita cargar n números enteros y luego nos informe cuántos valores
                                       fueron pares y cuántos impares.Emplear el operador “%” en la condición de la estructura condicional:
	                                   if (valor%2==0)         //Si el if da verdadero luego es par.*/
    {
        static void Main(string[] args)
        {
            int pares = 0;
            int impares = 0;
            int n = 0;
            int[] numeros = new int[]{ 15, 58, 21, 44, 87, 23, 56, 92, 41, 33 };
            while (n < 10)
            {
                Console.Write(numeros[n] + ", ");
                n++;
            }
            n = 0;
            while (n < 10)
            {
                if (numeros[n] % 2 == 0)
                    pares++;
                else
                    impares++;
                n++;
            }
            Console.WriteLine("");
            Console.WriteLine("Los números pares son: " + pares);
            Console.WriteLine("Los números impares son: " + impares);
        }
    }
}
