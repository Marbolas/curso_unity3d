﻿using System;

namespace Leccion08_Ejercicio05
{
    class Program   /**Escribir un programa que pida ingresar la coordenada de un punto en el plano,
                       es decir dos valores enteros x e y (distintos a cero).Posteriormente imprimir
                       en pantalla en que cuadrante se ubica dicho punto. 
                       (1º Cuadrante si x > 0 Y y > 0 , 2º Cuadrante: x < 0 Y y > 0, etc.) */
    {
        static void Main(string[] args)
        {
            int x, y;
            string linea;
            Console.WriteLine("Introduce la coordenada X");
            linea = Console.ReadLine();
            x = int.Parse(linea);
            Console.WriteLine("Introduce la coordenada Y");
            linea = Console.ReadLine();
            y = int.Parse(linea);

            if (x > 0 && y > 0)
                Console.WriteLine("Está en el primer cuadrante");
            else
            {
                if (x < 0 && y > 0)
                    Console.WriteLine("Está en el segundo cuadrante");
                else
                {
                    if (x < 0 && y < 0)
                        Console.WriteLine("Está en el tercer cuadrante");
                    else
                        Console.WriteLine("Está en el cuarto cuadrante");
                }
            }
        }
    }
}
