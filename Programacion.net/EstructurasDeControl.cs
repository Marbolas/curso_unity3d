using System;

public class EstructurasDeControl
{
	public static void Main()
	{	
		/*EjemploIfSimple();
		EjemploIfComplicado();
		EjercicioIfSumas();*/
		EjemploIfConsecutivos();
	}
	
	static void EjemploIfSimple()
	{
		//condicional simple: if (booleano) instrucción
		if(true)Console.WriteLine("Pues sí");
		if(false)Console.WriteLine("Pues va a ser que no");
		bool oSioNo = true;
		if(oSioNo)Console.WriteLine("Pues también sí");
		//o recibimos condicionales
		if(5 == 5)Console.WriteLine("Pues 5 == 5");
		if(4 > 7)Console.WriteLine("Pues esto tampoco se muestra");
	}
	
	static void EjemploIfComplicado()
	{
		//If complicado:  if (bool) instrucciónVerdad; else instrucciónFalso
		if(4 >= 7)Console.WriteLine("4>=7");
		else Console.WriteLine("4 < 7");
		//podemos separar en varias lineas
		if("Hola" != "hola")Console.WriteLine("Son distintos"); 
		else Console.WriteLine(" Son iguales");
	}
	
	static void EjercicioIfSumas()
	{
		//Ejercicio:
		string numA = "20", numB = "30", numC = "40";
		int resultado = 60;
		//suma las 3 combinaciones (A+B, B+C y A+C) y que el programa diga cual es igual a resultado
		
		int A = Int32.Parse(numA);
		int B = Int32.Parse(numB);
		int C = Int32.Parse(numC);
		int AmasB= A+B;
		int BmasC= B+C;
		int AmasC= A+C;
		if(AmasB == resultado)Console.WriteLine( "A = " + A + " + " + " B = " + B + " = Resultado " + resultado);
			else Console.WriteLine("");
		if(BmasC == resultado)Console.WriteLine( "B = " + B + " + " + " C = " + C + " = Resultado " + resultado);
			else Console.WriteLine("");
		if(AmasC == resultado)Console.WriteLine( "A = " + A + " + " + " C = " + C + " = Resultado " + resultado);
			else Console.WriteLine("");
	}
	static void EjemploIfConsecutivos()
	{
		Console.WriteLine("Introduzca una opción: ");
		Console.WriteLine(" 1 - opción primera ");
		Console.WriteLine(" 2 - opción segunda ");
		Console.WriteLine(" 3 - opción tercera ");
		Console.WriteLine(" 4 - Cualquier otra opción ");
		
		ConsoleKeyInfo opcion = Console.ReadKey();
		ConsoleKey conKey = opcion.Key;
		string caracter = conKey.ToString();
		//Console.WriteLine(">> " + caracter);
		Console.WriteLine("\n");
		
		//Si el caracter es un 1 del teclado normal o del teclado numérico
		if(caracter == "NumPad1" || caracter == "D1")
			Console.WriteLine(" Has elegido la primera ");
		else if(caracter == "NumPad2" || caracter == "D2")
			Console.WriteLine(" Has elegido la segunda ");
		else if(caracter == "NumPad3" || caracter == "D3")
			Console.WriteLine(" Has elegido la tercera ");
		else if(caracter == "NumPad4" || caracter == "D4")
			Console.WriteLine(" Has elegido la cuarta");
		else
			Console.WriteLine(" Ahhhhhhhhhh!!!! ");
	}
}