﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Leccion09_Ejercicio01
{
    class Leccion09_Ejercicio01   /**Escribir un programa que solicite ingresar 10 notas de alumnos
                    y nos informe cuántos tienen notas mayores o iguales a 7 y cuántos menores.*/
    {
        static void Main(string[] args)
        {
            float nota = 0;
            int x = 1;
            int contmayor = 0;
            int contmenor = 0;
            string linea;

            while(x <= 10)
            {
                Console.WriteLine("Introduce la nota" + x);
                linea = Console.ReadLine();
                nota = float.Parse(linea);

                if (nota >= 7)
                    contmayor++;
                else
                    contmenor++;
                x++;
            }
            Console.WriteLine("Alumnos con nota mayor o igual que 7 = " + contmayor);
            Console.WriteLine("Alumnos con nota menor 7 = " + contmenor);
            Console.ReadKey();
        }
    }
}
