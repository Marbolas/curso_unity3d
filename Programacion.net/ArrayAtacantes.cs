using System;

public class ArrayAtacantes
{
	
	//public float[] ataques = {3.2f,1.7f,2.4f,5.0f,7.1f,4,8f};
	//public bool[] cercanos = {false,true,true,false,true,true};
	
	public static void Main()
	{	
		EjercicioArrayAtaques();
	}
	public static void EjercicioArrayAtaques()
	{
		float[] ataques = {3.2f,1.7f,2.4f,5.0f,7.1f,4.8f};
		bool[] cercanos = {false,true,true,false,true,true};
		
		
		float totalAtaques = 0;
		float totalAtaque3 = 0;
		float valor = 3;
		float maximoAtaque = 0;
		
		
		totalAtaques = SumaAtaquesCercanos(ataques, cercanos);			//Ejer1
		Console.WriteLine("La suma de ataques es: " + totalAtaques);
		
		totalAtaque3 = AtaqueTotalMayor3(ataques, valor);
		Console.WriteLine("La suma de ataques mayor que tres es: " + totalAtaque3);	//Ejer3
		
		maximoAtaque = AtaqueMaximo(ataques);										//Ejer4
		Console.WriteLine("El máximo de ataque es: " + maximoAtaque);
		
		int numCercanos = 0;
		numCercanos = calcularTamañoCercanos(cercanos);		//calcula el tamaño del array cercanos
		
		string[] cercanosDepurado = new string[numCercanos];			
		cercanosDepurado = NuevoArrayNocercanos(ataques,cercanos,numCercanos);	//Devuelve el array nuevo de No cercanos
		mostrarArrayDepurado(cercanosDepurado);			//muestra el arrayDepurado

	}
	public static float SumaAtaquesCercanos(float[] ataques, bool[] cercanos)
	{
		//float[] ataques = {3.2f,1.7f,2.4f,5.0f,7.1f,4.8f};
		//bool[] cercanos = {false,true,true,false,true,true};
		
		float sumaAtaques = 0;
		for(int i = 0; i < ataques.Length; i++)
		{
			Console.WriteLine("valor de " + i + "es " + ataques[i]);
			if(cercanos[i] == true)
				sumaAtaques = sumaAtaques + ataques[i];
		}
		return sumaAtaques;
		//Console.WriteLine("La suma de ataques es: " + sumaAtaques);
	}	
	
	//Ejer 3: Crear una funcion que calcule el ataque total de los que tengan ataque > 3. Este tope debe parase por argumento
	
	public static float AtaqueTotalMayor3(float[] ataques, float valor)
	{
		float suma3 = 0;
		
		for(int i = 0; i < ataques.Length; i++)
			if(ataques[i] > valor)
				suma3 += ataques[i];
		return suma3;
	}
	
	//Ejer 4: Otra que devuelva el ataque máximo del array (resultado 7.1)
	
	public static float AtaqueMaximo(float[] ataques)
	{
		float maximo = 0;
		
		for(int i = 0; i < ataques.Length; i++)
			if(ataques[i] > maximo)
				maximo = ataques[i];
		return maximo;
	}
	
	//ejer 2: generar un array con textos con la info de los NO cercanos
	//El resultado es un array(){"Enemigo 0:3.2,"Enemigo 3:5.0f")
	
	public static string[] NuevoArrayNocercanos(float[] ataques,bool[] cercanos,int numcercanos)
	{		
		string[] nuevoCercanos = new string[numcercanos];
		string ataqueCercano ="";
		int j = 0;
		
		for(int i = 0; i < numcercanos ; i++)
		{
			if(cercanos[i] == false)
			{
				ataqueCercano = ataques[i].ToString();
				nuevoCercanos[j] = ataqueCercano;
				j++;
			}
		}
		return nuevoCercanos;
		/*or(int i2 =0; i2 < nuevoCercanos.Length; i2++)
			Console.WriteLine("Los valores de los atacantes NO cercanos son: " + nuevoCercanos[i2]);*/
	}
	
	public static int calcularTamañoCercanos(bool[] cercanos)		//Calcula el tamaño del array cercanos
	{
		int tamañoArrayCercanos = 0;
		for(int i3 = 0; i3 < cercanos.Length; i3++)
		{
				if(cercanos[i3] == false)
					tamañoArrayCercanos++;
		}		
		Console.WriteLine("El tamaño del array no cercanos es: " + tamañoArrayCercanos);
		return tamañoArrayCercanos;
	}
	
	public static void mostrarArrayDepurado(string[] cercanosDepurado)
	{
		Console.WriteLine(cercanosDepurado.Length);
		for(int i = 0; i < cercanosDepurado.Length; i++)
			Console.WriteLine("Enemigo " + i + ": " + cercanosDepurado[i]);
	}
}