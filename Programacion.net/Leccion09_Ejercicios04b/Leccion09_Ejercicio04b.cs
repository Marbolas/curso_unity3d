﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Leccion09_Ejercicio04b
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y;
            string linea;

            Console.WriteLine("Introduce x:");
            linea = Console.ReadLine();
            x = int.Parse(linea);
            Console.WriteLine("Introduce y:");
            linea = Console.ReadLine();
            y = int.Parse(linea);

            for (int i = 1; i <= x; i++)
                Console.Write(i * y + " - ");


            /*for (int contador = 1; contador <= 25; contador++)    //Ejercicio optimizadp
                Console.Write(contador *11 + " - ");*/
        }
        //Mejora 1.programación funcional

        public static void MostrarProgresion(int x,int y)
        {
            Console.WriteLine();
        }

    }
}