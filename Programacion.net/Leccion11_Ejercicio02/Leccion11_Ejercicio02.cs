﻿using System;

namespace Leccion11_Ejercicio02
{
    class Leccion11_Ejercicio02     /**En un banco se procesan datos de las cuentas corrientes de sus clientes. 
                                        De cada cuenta corriente se conoce: número de cuenta y saldo actual. 
                                        El ingreso de datos debe finalizar al ingresar un valor negativo en el número de cuenta.
                                        Se pide confeccionar un programa que lea los datos de las cuentas corrientes e informe:
                                        a)De cada cuenta: número de cuenta y estado de la cuenta según su saldo, sabiendo que:
                                          Estado de la cuenta	'Acreedor' si el saldo es >0.
			                                                    'Deudor' si el saldo es <0.
			                                                    'Nulo' si el saldo es =0.
                                        b) La suma total de los saldos acreedores. */
    {
        static void Main(string[] args)
        {
            int numcuenta = 0;
            float saldo = 0;
            float sumaacreedor = 0;
            string linea, linea2;

            do
            {
                Console.WriteLine("Introduce un número de cuenta: ");
                linea = Console.ReadLine();
                numcuenta = int.Parse(linea);

                if (numcuenta > 0)
                {
                    Console.WriteLine("Introduce el saldo: ");
                    linea2 = Console.ReadLine();
                    saldo = int.Parse(linea2);

                    if (saldo < 0)
                    {
                        Console.WriteLine("El numero de cuenta es: " + numcuenta);
                        Console.WriteLine("El saldo es: " + saldo);
                        Console.WriteLine("Es DEUDOR");
                    }
                    else
                    if (saldo > 0)
                    {
                        Console.WriteLine("El numero de cuenta es: " + numcuenta);
                        Console.WriteLine("El saldo es: " + saldo);
                        Console.WriteLine("Es ACREEDOR");
                        sumaacreedor = sumaacreedor + saldo;
                    }
                    else
                    {
                        Console.WriteLine("El numero de cuenta es: " + numcuenta);
                        Console.WriteLine("El saldo es: " + saldo);
                        Console.WriteLine("Es NULO");
                    }
                }
            } while (numcuenta > 0);

            Console.WriteLine("La suma total de acreedores es: " + sumaacreedor);
        }
    }
}
