﻿using System;

namespace Leccion10_Ejercicio02
{
    class Leccion10_Ejercicio02       /**Desarrollar un programa que solicite la carga de 10 números e imprima la suma de los últimos
                                        5 valores ingresados. */
    {
        static void Main(string[] args)
        {
            string linea;
            float num = 0;
            float sumaultimos = 0;

            for (int i = 0; i < 10; i++)
            {
            Console.WriteLine("Introduce el numero: " + (i+1));
            linea = Console.ReadLine();
            num = float.Parse(linea);
            if (i > 4)
                sumaultimos = sumaultimos + num;
            }
            Console.WriteLine("La suma de los 5 últimos números es: " + sumaultimos);
        }
    }
}
