﻿using System;

namespace Leccion06_Ejercicio03
{
    class Program   /**Confeccionar un programa que permita cargar un número entero positivo de hasta tres cifras
                    y muestre un mensaje indicando si tiene 1, 2, o 3 cifras.
                    Mostrar un mensaje de error si el número de cifras es mayor.*/
    {
        static void Main(string[] args)
        {
            int num;
            string linea;
            Console.WriteLine("Introduce un numero máximo de 3 cifras: ");
            linea = Console.ReadLine();
            num = int.Parse(linea);

            while (num >= 999)
            {
                Console.WriteLine("Introduce un numero máximo de 3 cifras");
                linea = Console.ReadLine();
                num = int.Parse(linea);

                if (num >= 999)
                    Console.WriteLine("ERROR, numero con mas de 3 cifras");

            }
            if (num < 10)
                Console.WriteLine("El nùmero:" + num + " tiene una cifra");
            else
                if(num < 100)
                    Console.WriteLine("El nùmero:" + num + " tiene dos cifras");
                else
                Console.WriteLine("El nùmero:" + num + " tiene tres cifras");
        }
    }
}
