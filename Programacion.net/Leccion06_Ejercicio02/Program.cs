﻿using System;

namespace Leccion05_Ejercicio02
{
    class Program       //Se ingresan tres notas de un alumno, si el promedio es mayor o igual a siete
                        //mostrar un mensaje "Promocionado". 
    {
        static void Main(string[] args)
        {
            float nota1, nota2, nota3, promedio;
            string linea;
            Console.Write("Ingrese primera nota:");
            linea = Console.ReadLine();
            nota1 = float.Parse(linea);
            Console.Write("Ingrese segunda nota:");
            linea = Console.ReadLine();
            nota2 = float.Parse(linea);
            Console.Write("Ingrese tercera nota:");
            linea = Console.ReadLine();
            nota3 = float.Parse(linea);
            promedio = (nota1 + nota2 + nota3) / 3;
            if (promedio > 7)
                Console.Write("PROMOCIONADO nota = " + promedio);
         
            else
                Console.Write(" NO PROMOCIONADO nota = " + promedio);
     
        }
    }
}