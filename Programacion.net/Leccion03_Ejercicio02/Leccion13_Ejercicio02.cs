﻿using System;

namespace Leccion13_Ejercicio02
{
    class OperacionNumeros      /**Implementar la clase operaciones. Se deben cargar dos valores enteros,
                                   calcular su suma, resta, multiplicación y división, cada una en un método,
                                   imprimir dichos resultados.*/
    {
        private float num, num2;
        private string linea;

        void Cargar()
        {
            Console.WriteLine("Introduce número 1: ");
            linea = Console.ReadLine();
            num = float.Parse(linea);
            Console.WriteLine("Introduce número 2:");
            linea = Console.ReadLine();
            num2 = float.Parse(linea);
        }

        void Sumar()
        {
            float suma = num + num2;
            Console.WriteLine("La suma es: " + suma);
        }
        void Restar()
        {
            float resta = num - num2;
            Console.WriteLine("La resta es: " + resta);
        }
        void Multiplicar()
        {
            float multiplicacion = num * num2;
            Console.WriteLine("La multiplicación es: " + multiplicacion);
        }
        void Dividir()
        {
            float division = num / num2;
            Console.WriteLine("La división es: " + division);
        }


        static void Main(string[] args)
        {
            OperacionNumeros opnum = new OperacionNumeros();
            opnum.Cargar();
            opnum.Sumar();
            opnum.Restar();
            opnum.Multiplicar();
            opnum.Dividir();
        }
    }
}
