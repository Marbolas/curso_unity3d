﻿using System;
using System.Collections.Generic;

//    

namespace Leccion11_Ejemplo
{
    class Program
    {
        // Ahora lo que hace es leer el array y meter en dos listas los strings que 
        // contienen Clone  y los que no, usando  Contains()
        // Ejercicio: Similar, meter en dos listas los de 1 y los de 2 usando IndexOf()
        static void Main(string[] args)
        {   string[] nombresEnemigos =
            {
                "Lata 1",           // Objetos de la escena precargados
                "Lata 2 (Clone)", // Objetos de la escena instanciados por código, osea, dinámicos
                "Lata 2 (Clone)",
                "Lata 2 (Clone)",
                "Lata 2",
                "Basura 1",
                "Basura 2"
            };

            string dosPrimeros = "Los dos primeros enemigos son " + nombresEnemigos[0] + ", " + nombresEnemigos[1];
            Console.WriteLine(dosPrimeros);
            List<string> objPre = new List<string>();
            List<string> objClo = new List<string>();

            for (int i = 0; i < nombresEnemigos.Length; i++)
            {
                string enemigoActual = nombresEnemigos[i];
                if (enemigoActual.Contains("(Clone"))
                {
                    objClo.Add(enemigoActual);
                } else
                {
                    objPre.Add(enemigoActual);
                }
            }
            Console.WriteLine("Clonados: " + objClo.Count);
            for (int i = 0; i < objClo.Count; i++)
            {
                string enemigoActual = objClo[i];
                Console.WriteLine("Clonado: " + enemigoActual);
            }
            Console.WriteLine("Precargados: " + objPre.Count);
            foreach (string enemAc in objPre)
            {
                Console.WriteLine("Precargado: " + enemAc);
            }

        }
    }
}
