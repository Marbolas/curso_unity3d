﻿using System;

namespace Leccion09_Ejercicio_05
{
    class Leccion09_Ejercicio05       /**Mostrar los múltiplos de 8 hasta el valor 500. Debe aparecer en pantalla 8 - 16 - 24, etc. */         
    {
        static void Main(string[] args)
        {
            int num = 8;
           
            while(num <= 500)
            {
                Console.WriteLine(num + ", ");
                num = num + 8;
            }
            Console.ReadKey();
        }
    }
}
