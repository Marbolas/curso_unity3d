﻿using System;

namespace Leccion10_Ejercicio07
{
    class Leccion10_Ejercicio07     /**Se realiza la carga de 10 valores enteros por teclado. Se desea conocer:
                                        a) La cantidad de valores ingresados negativos.
                                        b) La cantidad de valores ingresados positivos.
                                        c) La cantidad de múltiplos de 15.
                                        d) El valor acumulado de los números ingresados que son pares. */
    {
        static void Main(string[] args)
        {
            int contnegativos = 0;
            int contpositivos = 0;
            int contmultip15 = 0;
            int sumapares = 0;

            int[] numeros = new int[] { -15, 58, -21, 44, 60, 23, -56, 92, 150, 33 };

            for (int j = 0; j < 10; j++)
            {
                Console.Write(numeros[j] + ", ");
            }

            for (int i = 0; i < 10; i++)
            {
                if (numeros[i] < 0)
                    contnegativos++;
                else
                    contpositivos++;

               

                if (numeros[i] > 0 && numeros[i] % 2 == 0)
                    sumapares = sumapares + numeros[i];
            }

            Console.WriteLine(" ");
            Console.WriteLine("Nº de valores negativos: " + contnegativos);
            Console.WriteLine("Nº de valores positivos: " + contpositivos);

            Console.WriteLine("Valores multiplos de 15: ");
            for (int i = 0; i < 10; i++)           //mostrar los múltiplos de 15
            {
                if (numeros[i] % 15 == 0)
                {
                    contmultip15++;
                    Console.Write(numeros[i] + ", ");
                }
            }
            Console.WriteLine(" ");
            Console.WriteLine("Nº de valores multiplos de 15: " + contmultip15);

            
            Console.WriteLine("La suma de los números pares es: " + sumapares);
        }
    }
}
