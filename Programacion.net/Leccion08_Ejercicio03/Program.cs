﻿using System;

namespace Leccion08_Ejercicio03
{
    class Program   /**Se ingresan por teclado tres números, si todos los valores ingresados son menores a 10,
                       imprimir en pantalla la leyenda "Todos los números son menores a diez".*/
    {
        static void Main(string[] args)
        {
            int num, num2, num3;
            string linea;
            Console.WriteLine("Introduce el 1º número");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            Console.WriteLine("Introduce el 2º número");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.WriteLine("Introduce el 3º número");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);

            if (num < 10 && num2 < 10 && num3 < 10)
                Console.WriteLine("Todos los números son menores de 10: " + num + " , " + num2 + " , " + num3);
            else
                Console.WriteLine("Todos los números no son menores de 10: " + num + " , " + num2 + " , " + num3);
        }
    }
}
