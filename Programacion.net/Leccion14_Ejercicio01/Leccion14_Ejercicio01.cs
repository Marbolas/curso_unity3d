﻿using System;
using System.Runtime.CompilerServices;

namespace Leccion14_Ejercicio01
{
    class RaizCuadrada
    {
        void CargarValores()
        {
            Console.WriteLine("introduce el número: ");
            string linea = Console.ReadLine();
            float num = float.Parse(linea);
            float numraiz = CalcuRaizCuadrada(num);
            Console.WriteLine("La raiz cuadrada de " + num + " es " + numraiz);
        }
        public float CalcuRaizCuadrada(float x)
        {
            return (float)Math.Sqrt(x);
            //return pow(x * 0.5f);
        }
        static void Main(string[] args)
        {
            RaizCuadrada raiznum = new RaizCuadrada();
            raiznum.CargarValores();
        }
    }
}
