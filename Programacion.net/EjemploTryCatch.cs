﻿using System;

public class EjemploTryCatch
{
	static void Main()
	{
		//int x = Int32.parse("No es numero")	lanzara una excepcion
		try
		{
			Console.WriteLine("Empieza el programa");
			int x = Int32.Parse("No es numero");
			Console.WriteLine("Esta linea no se ejecuta");
		}

		catch (FormatException error)
		{
			Console.WriteLine("Podemos diferenciar entre tipos de errores");
			Console.WriteLine("Error de formato");
			Console.WriteLine(error.Message);
		}
		catch (Exception error)
		{
			Console.WriteLine("Pero al capturar el error,si permitimos continuar el programa");
			Console.WriteLine("Incluso mostrar el error de manera controlada");
			Console.WriteLine(error.Message);
		}

		Console.WriteLine("Termina el programa");
		EjercicioArrayTryCatch();
	}


	//provocar un error de limites de array, es decir creamos un array de 5 elementos e intentamos
	//acceder al 7º (que no exixte) controlarlo con try y catch
	static void EjercicioArrayTryCatch()
	{
		try
			{
			Console.WriteLine("");
			Console.WriteLine("Empieza el try and catch");
			int[] elementos = { 1, 2, 3, 4, 5 };
			elementos[7] = 7;
			Console.WriteLine(elementos[7]);
			}

		catch(IndexOutOfRangeException error)
			{
			Console.WriteLine(error.Message);
			}

		catch (Exception error)
			{
			Console.WriteLine("Has excedido el tamaño del array");
			Console.WriteLine(error.Message);
			Console.WriteLine("Final del programa");
			}
	}
}
