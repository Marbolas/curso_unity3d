﻿using System;

namespace Leccion06_Ejercicio01
{
    class Program   //Se cargan por teclado tres números distintos.Mostrar por pantalla el mayor de ellos.
    {
        static void Main(string[] args)
        {
            float num1, num2, num3;
            string linea;
            Console.WriteLine("introducir numero 1");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.WriteLine("introducir numero 2");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.WriteLine("introducir numero 3");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
                
            if(num1 > num2 && num1 > num3)
                Console.WriteLine("El numero mayor de: " + num1 + " , " + num2 + " ,y " + num3 + " es: " + num1);
            else
                if (num2 > num1 && num2 > num3)
                Console.WriteLine("El numero mayor de: " + num1 + " , " + num2 + " ,y " + num3 + " es: " + num2);
                else
                    //(num1 < num2 && num1 < num3)
                    Console.WriteLine("El numero mayor de: " + num1 + " , " + num2 + " ,y " + num3 + " es: " + num3);
        }
    }
}
