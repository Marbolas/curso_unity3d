﻿using System;

namespace Leccion10_Ejercicio05
{
    class Leccion10_Ejercicio05       /**Realizar un programa que lea los lados de n triángulos, e informar:
                           a) De cada uno de ellos, qué tipo de triángulo es: equilátero (tres lados iguales), isósceles (dos lados iguales),
                              o escaleno (ningún lado igual)
                           b) Cantidad de triángulos de cada tipo.
                           c) Tipo de triángulo que posee menor cantidad. */
    {
        static void Main(string[] args)
        {
            float lado, lado2, lado3;
            int contequilateros = 0;
            int contisosceles = 0;
            int contescalenos = 0;
            string linea;

            for(int i = 0; i < 5; i++)
            {
                Console.WriteLine("Introduce el primer lado: ");
                linea = Console.ReadLine();
                lado = int.Parse(linea);
                Console.WriteLine("Introduce el 2º lado: ");
                linea = Console.ReadLine();
                lado2 = int.Parse(linea);
                Console.WriteLine("Introduce el tercer lado: ");
                linea = Console.ReadLine();
                lado3 = int.Parse(linea);

                if(lado == lado2 && lado == lado3)
                {
                    Console.WriteLine("El triángulo es equilatero: ");
                    contequilateros++;
                }
                else if(lado == lado2 && lado2 != lado3 || lado == lado3 && lado3 != lado2 || lado2 == lado3 &&  lado2 != lado3)
                {
                Console.WriteLine("El triángulo es isósceles: ");
                contisosceles++;
                }
                else if (lado != lado2 && lado != lado3 && lado2 != lado3)
                {
                    Console.WriteLine("El triángulo es escaleno: ");
                    contescalenos++;
                }
            }
            Console.WriteLine("Total triángulos equilateros: " + contequilateros);
            Console.WriteLine("Total triángulos isosceles: " + contisosceles);
            Console.WriteLine("Total triángulos escalenos: " + contescalenos);

            if (contequilateros < contescalenos && contequilateros < contisosceles)
            {
                Console.WriteLine("Hay menos equilateros: " + contequilateros);
            }
            else if (contisosceles < contescalenos && contisosceles < contequilateros)
                Console.WriteLine("Hay menos isosceles: " + contisosceles);
            else if (contescalenos < contequilateros && contescalenos < contisosceles)
                Console.WriteLine("Hay menos escalenos: " + contescalenos);

            if (contequilateros == contisosceles)
            {
                Console.WriteLine("el número de equilateros e isósceles es igual: " + contequilateros + " = " + contisosceles);
            }
            else if (contequilateros == contescalenos)
                Console.WriteLine("el número de equilateros y escalenos es igual: " + contequilateros + " = " + contescalenos);
            else if (contescalenos == contisosceles)
                Console.WriteLine("el número de escalenos e isósceles es igual: " + contescalenos + " = " + contisosceles);
        }
    }
}
