﻿using System;

namespace Leccion08_Ejercicio02
{
    class Program   /**Se ingresan tres valores por teclado, si todos son iguales se imprime la suma del primero
                       con el segundo y a este resultado se lo multiplica por el tercero. */
    {
        static void Main(string[] args)
        {
            string linea;
            int num, num2, num3,resultado;

            Console.WriteLine("Introduce el 1º número");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            Console.WriteLine("Introduce el 2º número");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.WriteLine("Introduce el 3º número");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);

            if (num == num2 && num == num3)
            {
                resultado = (num + num2) * num3;
                Console.WriteLine("número1: " + num + " + número2: " + num2 + " * número3: " + num3 + " = " + resultado);
            }
            else
                Console.WriteLine("Los números no son iguales");
        }
    }
}
