﻿using System;

public class PasoPorValorPorReferencia
{
	static void Main()
	{
		Console.WriteLine("Paso por valor:");
		int y = 10;
		CambiarVariablePorValor(y);
		Console.WriteLine("Y = " + y);

		CambiarVariablePorReferencia(out y);
		Console.WriteLine("Y = " + y);
		string SupuestoNumero = "200", SupuestoNumero2 = "L00";
		int numero_1 = 0;
		int numero_2 = 0;

		//Ejemplo de paso por valor
		//Como puede fallar hay que controlar la excepsion con try / catch
		try
			{	
			numero_1 = Int32.Parse(SupuestoNumero);
			numero_2 = Int32.Parse(SupuestoNumero2);
			Console.WriteLine("Num 1 = " + numero_1 + "Num 2 = " + numero_2);
			}
		catch(FormatException error)
			{
			Console.WriteLine(" Fallo de formato " + "numero_1 = " +numero_1 + " El numero_2 =" + SupuestoNumero2);
			Console.WriteLine(error.Message);
			}

		//Ejemplo de paso por  referencia
		//if(int32.Parse)

	}
	//cambiar variable por valor
	static void CambiarVariablePorValor(int x)
    {
		x = 20;
    }

	static void CambiarVariablePorReferencia(out int z)
	{
		z = 20;
	}
}
