﻿using System;

namespace Leccion08_Ejercicio04
{
    class Program   /**Se ingresan por teclado tres números, si al menos uno de los valores ingresados es menor a 10,
                       imprimir en pantalla la leyenda "Alguno de los números es menor a diez".*/
    {
        static void Main(string[] args)
        {
            int num, num2, num3;
            string linea;
            Console.WriteLine("Introduce el 1º número");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            Console.WriteLine("Introduce el 2º número");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.WriteLine("Introduce el 3º número");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);

            if (num < 10 || num2 < 10 || num3 < 10)
                Console.WriteLine("Alguno de los números es menor de 10: " + num + " , " + num2 + " , " + num3);
            else
                Console.WriteLine("Ninguno de los números es menor de 10: " + num + " , " + num2 + " , " + num3);
        }
    }
}
