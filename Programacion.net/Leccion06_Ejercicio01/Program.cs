﻿using System;

namespace Leccion05_Ejercicio01
{
    class Program      /**Realizar un programa que lea por teclado dos números, si el primero es mayor al segundo informar su suma y 
                          diferencia, en caso contrario informar el producto y la división del primero respecto al segundo. */
    {
        static void Main(string[] args)
        {
            float num1, num2, suma, resta, producto, division;
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1 = float.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            num2 = float.Parse(linea);
            suma = num1 + num2;
            resta = num1 - num2;
            producto = num1 * num2;
            division = num1 / num2;

            if (num1 > num2)
            {
                Console.WriteLine("La suma de " + num1 + " + " + num2 + " = " + suma);
                Console.Write("La resta de " + num1 + " - " + num2 + " = " + resta);
            }
            else
            {
                Console.WriteLine("El producto de " + num1 + " * " + num2 + " = " + producto);
                Console.Write("La division de " + num1 + " / " + num2 + " = " + division);
            }
            Console.ReadKey();
        }
    }
}
