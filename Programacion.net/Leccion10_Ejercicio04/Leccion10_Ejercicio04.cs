﻿using System;

namespace Leccion10_Ejercicio04
{
    class Leccion10_Ejercicio04     /**Confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre la tabla de multiplicar
                                       del mismo (los primeros 12 términos).Ejemplo: Si ingreso 3 deberá aparecer en pantalla los valores
                                       3, 6, 9, hasta el 36. */
    {
        static void Main(string[] args)
        {
            int num, valor;
            string linea;

            Console.WriteLine("Introduce un numero del 1 al 10: ");
            linea = Console.ReadLine();
            num = int.Parse(linea);

            for (int i = 1; i <= 12; i++)
            {
                valor = num * i;
                Console.WriteLine(num + " * " + i + " = " + valor);
            }
        }
    }
}
