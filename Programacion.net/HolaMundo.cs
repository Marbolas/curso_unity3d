// Al principio se ponen las importaciones
using System;

public class HolaMundo
{
	static void Main()
	{	
		Console.Beep();
		{
		Console.WriteLine("Hola mundo!");
		Console.WriteLine("¿Qué tal?");
		Console.ReadKey();
		}
		Console.WriteLine("Pues bien");
		Console.Beep();
		//Tipo nombre ;
		byte unNumero;	//byte de 0 a 255		
		unNumero = 10;
		Console.WriteLine("El byte es " + unNumero);
		
		char unCaracter;	//ocupa 1 o 2 bytes
		unCaracter = 'A';	//Comillas simples para caracteres
							//Comillas dobles para textos de varios caracteres
		Console.WriteLine("El caracter es " + unCaracter);
		//Un caracter también es un número
		unCaracter = (Char) (65 + 4);	//un char es un número, pero para hacer
								//la conversión hay que hacer "casting"
		Console.WriteLine("El caracter es " + unCaracter);
	}
}