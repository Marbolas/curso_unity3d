﻿using System;
using System.Runtime.CompilerServices;

namespace Leccion13_Ejercicio01
{
    class Empleado      /**Confeccionar una clase que represente un empleado. Definir como atributos su nombre y su sueldo. 
                           Confeccionar los métodos para la carga, otro para imprimir sus datos y por último uno que imprima
                           un mensaje si debe pagar impuestos (si el sueldo supera a 3000)*/ 
    {
        private string nombre;
        private float sueldo;
        private string linea;

         void Cargar()
        {
            Console.WriteLine("Introduce nombre:");
            linea = Console.ReadLine();
            nombre = linea;
            Console.WriteLine("Introduce sueldo:");
            sueldo = float.Parse(Console.ReadLine());
        }

         void Imprimir()
        {
            Console.WriteLine("El nombre es: " + nombre);
            Console.WriteLine("El sueldo es: " + sueldo);
            PagarImpuestos();
        }

        void PagarImpuestos()
        {
            if (sueldo > 3000)
                Console.WriteLine("Debe pagar impuestos");
        }
        static void Main(string[] args)
        {

            Empleado emp = new Empleado();
            emp.Cargar();
            emp.Imprimir();
        }
    }
}
