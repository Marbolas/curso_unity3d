﻿using System;

namespace Leccion18_Ejercicio01
{
    class VectorMayorMenor      //Cargar un vector de n elementos. imprimir el menor y un mensaje si se repite dentro del vector.
    {
        private int[] vectorNum;
        private int tam = 0;
        private int menor = 0;
        private int cont = 0;
        static void Main(string[] args)
        {
            VectorMayorMenor vecmaymen = new VectorMayorMenor();
            vecmaymen.CargarVector();
            vecmaymen.BuscarMenor();
            //vecmaymen.RepetirMenor();
            vecmaymen.Imprimir();
        }

        public void CargarVector()
        {
            //int tam = 0;
            int valor = 0;
            
            Console.WriteLine("Introduce el tamaño del vector:");
            tam = int.Parse(Console.ReadLine());
            vectorNum = new int[tam];

            for (int i = 0; i < tam; i++)
            {
                Console.WriteLine("Introduce un número:");
                valor = int.Parse(Console.ReadLine());
                vectorNum[i] = valor;
            }
        }

        public void BuscarMenor()
        {
            menor = vectorNum[0];

            for (int i = 0; i < tam; i++)
            {
                if(vectorNum[i] <= menor)
                {
                    menor = vectorNum[i];
                    //if (vectorNum[i] == menor)
                        //cont++;
                }
            }
            RepetirMenor();
        }

        public void RepetirMenor()
        {
            for(int i = 0; i < tam; i++)
            {
                if (vectorNum[i] == menor)
                    cont++;
            }
        }

        public void Imprimir()
        {
            Console.WriteLine("El número menor es: " + menor);
            Console.WriteLine("El número menor se repite " + cont + " veces.");
        }
    }
}
