﻿using System;

namespace Leccion15_Ejercicio04
{
    class VectorOrdenado       /**Cargar un vector de 10 elementos y verificar posteriormente si el mismo está ordenado de menor a mayor.*/
    {

        private int[] vectorord;

        public void Cargar()
        {
            vectorord = new int[10];
            for(int i= 0;i <10; i++)
            {
                Console.WriteLine("Introduce elemento:");
                vectorord[i] = int.Parse(Console.ReadLine());
            }
        }

        public void Verificar()
        {
            bool comparar = true;

            for(int i = 0; i < 9; i++)
            {
                if (vectorord[i + 1] < vectorord[i])
                    comparar = false;
            }

            if (comparar == true)
                Console.WriteLine("El vector está ordenado");
                else
                Console.WriteLine("El vector está desordenado");
        }
            static void Main(string[] args)
        {
            VectorOrdenado vord = new VectorOrdenado();
            vord.Cargar();
            vord.Verificar();
        }
    }
}
