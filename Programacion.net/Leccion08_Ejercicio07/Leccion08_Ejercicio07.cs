﻿using System;

namespace Leccion08_Ejercicio07
{
    class Leccion08_Ejercicio07     /**Escribir un programa en el cual: dada una lista de tres valores numéricos distintos
                                       se calcule e informe su rango de variación (debe mostrar el mayor y el menor de ellos) */
    {
        static void Main(string[] args)
        {
            float num, num2, num3, rangovariacion;
            float mayor = 0;
            float menor = 0; 
            string linea;

            Console.WriteLine("Introduce el número 1: ");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            Console.WriteLine("Introduce el número 2: ");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.WriteLine("Introduce el sueldo");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            if (num > num2 && num > num3)
            {
                mayor = num;
                if (num2 > num3)
                    menor = num3;
                else
                    menor = num2;
            }
            else if (num2 > num && num2 > num3)
            {
                mayor = num2;
                if (num > num3)
                    menor = num3;
                else
                    menor = num;
            }
            else if (num3 > num && num3 > num2)
            {
                mayor = num3;
                if (num > num2)
                    menor = num2;
                else
                    menor = num;
            }

            rangovariacion = mayor - menor;
            Console.WriteLine("El rango de variación es: " + rangovariacion);
            Console.WriteLine("El mayor es : " + mayor);
            Console.WriteLine("El menor es : " + menor);
        }
    }
}
