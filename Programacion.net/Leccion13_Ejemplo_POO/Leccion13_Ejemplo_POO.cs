﻿using System;
// Mini juego: 
// Fase 1: Como jugadores tenemos una unidad, y luchamos contra 1 enemigo

namespace Leccion13_Ejemplo_POO
{
    // La clase Program la hacemos static para que NO se pueda instaciar ni usar como objeto.
    static class Program
    {
        /* Ejercicios:
         * 1) A la hora de atacar, comprobar que quien ataca está vivo (si está muerto no puede atacar)         
         *          En Unidad -> AtacaA()
         *   Extra: No atacar si a quién ataca está también. Es decir, si cualquiera está muerto (quién ataca o a quién se ataca) no se puede atacar. 
         *   Que la vida no puede ser negativa
         * 2) Meter un segundo enemigo: El jugador ataca a los 2 enemigos, y estos atacan al jugador     
         *          En el Juego -> Varios métodos
         * 3) Nuevo campo en Unidad pocion de tipo entero, habrá que añadir en 
         *    los constructores, dar un valor...
         *          En Unidad
         * 4) Crear una vidaMax, que guarde la vida inicial.
         *          En Unidad
         * 5) Un nuevo método CurarA(Un) para que una unidad cure a otra, en función del valor que tenga como poción. 
         *          En Unidad
         * 6) Hacer que cuando cure, no sobrepase la vidaMax. Sólo si está vivo
         *          En el método Unidad -> CurarA()
         * 7) Hacer que el enemigo 1 cure al 2, por ejemplo, dos veces
         *          En el Juego
         * 8) Que el juego se repita hasta que ganes o pierdas. 
         *          En Main()
         * 9) Hacer que haya tres enemigos con opcion ha que hallan 30. Arrays o listas
         * */
        static void Main(string[] args)
        {
            Console.WriteLine("Juego!");
            Juego miJuego = new Juego();
            miJuego.Inicializar();
            miJuego.MostrarUnidades();
            do
            {
                miJuego.RealizarAtaques();
                miJuego.MostrarUnidades();

                miJuego.CurarUnidad();
                miJuego.CurarUnidad();
                miJuego.MostrarUnidades();

                //miJuego.RealizarAtaques();
                //miJuego.MostrarUnidades();

            } while (!miJuego.SiGameOver() && !miJuego.SiGameWin());

        }
    }
}
