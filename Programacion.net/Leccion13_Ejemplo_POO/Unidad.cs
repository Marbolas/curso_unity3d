﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Leccion13_Ejemplo_POO
{
    class Unidad
    {
        private const int JJJ = 10;
        // La encapsulación de la POO es esto: usar la 
        // posibilidad de poner en private las propiedades

        private String nombre;
        private int vida;
        private int ataque;
        private int vidaMax;
        private int pocion;

        const int SIN_VIDA = 0;
        #region Constructores:
        // Constructores: Son métodos especiales para construir, 
        // No devuelven nada concreto, sólo un objeto del tipo de la propia clase
        // Constructor por defecto:
        public Unidad()
        {
            this.nombre = "Unidad sin nombre";
            this.vida = 100;
            this.ataque = 10;
            this.vidaMax = this.vida;
            this.pocion = 20;
        }   //end unidad
        // Crear un constructor donde la vida por defecto se ponga a 100
        public Unidad(String nombre, int ataque, int pocion)
        {
            this.nombre = nombre;
            this.vida = 100;
            this.ataque = ataque;
            this.vidaMax = this.vida;
            this.pocion = pocion;
        }   //end unidad
        // Constructor con parámetros
        public Unidad(String nombre, int nuevaVida, int ataque, int pocion)
        {
            this.nombre = nombre;
            this.vida = nuevaVida;
            this.ataque = ataque;
            this.vidaMax = this.vida;
            this.pocion = pocion;
        }//end unidad
        #endregion

        #region Getters y Setters 
        public int GetVidaMaxima()
        {
            return this.vidaMax;
        }   //end GetVidaMaxima
        public void SetVidaMaxima(int vm)
        {
            // Si lo necesitamos, en los Getter y/o Setter podemos cambiar el 
            // el comportamiento de acceso a la propiedad, por ejemplo, validando 
            // los datos
            if (vm < 0)
                vm = 0;
            if (this.vida > vm)
                this.vida = vm;
            this.vidaMax = vm;
        }   //end SetVidaMaxima
        public string GetNombre()
        {
            return this.nombre;
        }//end GetNombre
        public void SetNombre(string nom)
        {
            if (nom == "" || nom == null)   // Si el nuevo nombre está vació, entonces
            {
                // Provocamos un error (ecepción)
                throw new Exception("Eh! Te has colado, no puede ser vacío");
            }
            this.nombre = nom;
        } //end SetNombre

          //ATaque con formato getter y setter de c#
          //Nombre de variable miembro la primera con mayúscula
        public int Ataque
        {
            get
            {
            return this.ataque;
            }
            set //en el setter value corresponde al valor asignado
                {
                 if (value < 0)
                    value = 0;
                    this.ataque = value;
                }
        }//end Ataque

        public int Pocion
        {
            get
            {
                return this.pocion;
            }
            set //en el setter value corresponde al valor asignado
            {
                if (value < 0)
                    value = 0;
                this.pocion = value;
            }
        }//end pocion

        public int Vida
        {
            get
            {
                return this.vida;
            }
            set //en el setter value corresponde al valor asignado
            {
                if (value < 0)
                    value = 0;
                this.vida = value;
            }
        }//end Vida
        #endregion

        #region Métodos de visualización
        public String EnTexto()
        {
            return " * Nombre:" + this.nombre + ", Vida/Ataque: " + this.vida + "/" + this.ataque;
        }//end Entexto
        public void Mostrar(String tipoUnidad)
        {
            Console.WriteLine(" * Unidad " + tipoUnidad + " :" + this.nombre);
            Console.WriteLine("   Vida/Ataque: " + this.vida + "/" + this.ataque);
        }//end Mostrar
        #endregion

        #region Métodos de juego

        /*  public void RecibirAtaque(int x_ataque)
          {
              this.vida = this.vida - x_ataque;
          }*/
        // Crear un método AtacaA(), que quite X vida a una unidad (parámetro) según el ataque del propio objeto (this.ataque)
        public void AtacaA(Unidad unidRecibe)
        {
            if (this.EstaVivo() && unidRecibe.EstaVivo())
            {
                unidRecibe.vida = unidRecibe.vida - this.ataque;

                if (!unidRecibe.EstaVivo())
                {
                    unidRecibe.vida = SIN_VIDA;
                }//end if
                Console.WriteLine(this.nombre + "ataca a " + unidRecibe.nombre
                + " (" + unidRecibe.vida + ") "
                /* + " COMO Método de clase" */ );
            }
            else
            {
                if (!mensajeMostrado)
                {
                    Console.WriteLine(this.nombre + " No puede atacar porque alguien está muerto");
                    mensajeMostrado = true;
                }//end if
            }
        }//end AtacaA
        bool mensajeMostrado;

        public void CurarA(Unidad unidad)
        {
            if (EstaVivo() && unidad.EstaVivo())
            {
                unidad.vida += this.pocion;
                if (unidad.vida > unidad.vidaMax)
                {
                    unidad.vida = unidad.vidaMax;
                }//end if
                Console.WriteLine(" >>>" + nombre + " ha curado a " + unidad.nombre
                    + "(" + unidad.vida + ")");
            }   //end if
        }   //endCurarA

        public bool EstaVivo()
        {
            if (this.vida > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }   //end EstaVivo
        #endregion
    }    //end class
}   //end namespace