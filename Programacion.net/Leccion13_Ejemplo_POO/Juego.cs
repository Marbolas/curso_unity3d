﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Leccion13_Ejemplo_POO
{
    class Juego
    {
        public List<Unidad> enemigos;
        public Unidad jugador = null;
        //public Unidad enemigo = null;
        //public Unidad enemigo2 = null;

        #region Inicialización y visualización
        public void Inicializar()
        {
            //La lista es un objeto y hay que darle un valor de lista vacía

            this.enemigos = new List<Unidad>();
            this.enemigos.Add(new Unidad("Ctchulu", 100, 10, 20));
            //this.enemigo = new Unidad("Ctchulu", 100, 10, 20);
            this.enemigos.Add(new Unidad("Gollum", 10, 20));
            Unidad otroEnem3 = new Unidad("Furia", 18, 20);
            this.enemigos.Add(otroEnem3);

            for(int i =0; i < 3;i++)
            {
                int nuevaVida = new Random().Next(10, 15);
                int nuevoAtaque= new Random().Next(5, 10);
                int nuevaPocion = new Random().Next(0, 2);
                otroEnem3 = new Unidad("Enem" + i,
                    nuevaVida, nuevoAtaque, nuevaPocion);
                this.enemigos.Add(otroEnem3);
            }
            
            // Crear otro enemigo, cuya vida sea 100, con otro nombre y ataque. DEPURAR para COMPROBAR
            this.jugador = new Unidad();
            // Console.WriteLine("Dí tu nombre:");
            this.jugador.SetNombre ("Neo"); // Console.ReadLine();
            // Console.WriteLine("Indica tu ataque:");
            // int.Parse(Console.ReadLine());
            this.jugador.Ataque = 60;
            this.jugador.Vida = 40;
            this.jugador.Pocion = 20;
        }
        public void MostrarUnidades()
        {
            Console.WriteLine("");
            // jugador.Mostrar("JUGADOR");
            Console.WriteLine(jugador.EnTexto());

            for(int i = 0; i < enemigos.Count; i++)
            {
                enemigos[i].Mostrar("ENEMIGO");
            }
            //enemigo.Mostrar("ENEMIGO"); 
            //enemigo2.Mostrar("ENEMIGO2");
            Console.WriteLine("");
            // Console.WriteLine(enemigo.EnTexto());
        }
        #endregion

        #region Lógica de juego
        // Programar un método: ComenzarAtaques(), que haga que enemigo ataque a jugador, y jugador a enemigo.
        public void RealizarAtaques()
        {
            this.jugador.AtacaA(this.enemigos[0]);
            this.jugador.AtacaA(this.enemigos[1]);
            this.enemigos[0].AtacaA(this.jugador);
            this.enemigos[1].AtacaA(this.jugador);
        }

        public void CurarUnidad()
        {
            int ene1 = 0;
            int ene2 = 0;

           do
            {
                ene1 = new Random().Next(0, 3);
                ene2 = new Random().Next(0, 3);
            } while (ene1 == ene2);

            this.enemigos[ene1].CurarA(this.enemigos[ene2]);
            //this.enemigo.CurarA(this.enemigo2);
        }
        public bool SiGameOver()
        {
            // if (UnidadEstaViva(this.jugador))
            if (this.jugador.EstaVivo())
            {
                Console.WriteLine("SIGUE VIVO");
                return false;
            }
                
            else
            {
                Console.WriteLine("Jugador ha muerto");
                Console.WriteLine("GAME OVER!");
                return true;
            }

        }
        public bool SiGameWin()
        {
            bool enemigoVivo = false;

            if (this.enemigo.EstaVivo() == false && this.enemigo2.EstaVivo() == false && this.jugador.EstaVivo())
            {
                Console.WriteLine(this.jugador.GetNombre ()+ " HA GANADO");
                return true;
            }
            else
            {
                Console.WriteLine("SEGUIMOS LUCHANDO");
                return false;
            }
        }
        #endregion

        #region Métodos estáticos: NO USAR
        /* public static void Unidad_1_Ataca_A_Unidad_2(Unidad unidadQueAtaca, Unidad unidadQueRecibe)
         {
             unidadQueRecibe.vida = unidadQueRecibe.vida - unidadQueAtaca.ataque;


             Console.WriteLine(unidadQueAtaca.nombre + " ataca a " + unidadQueRecibe.nombre + " desde el JUEGO");
         }
         public static bool UnidadEstaViva(Unidad unidad)
         {
             if (unidad.vida > 0)
             {
                 return true;
             } else
             {
                 return false;
             }
         }
        */
        #endregion

    }
}
