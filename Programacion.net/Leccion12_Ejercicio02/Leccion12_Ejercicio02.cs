﻿using System;
using System.Collections.Generic;

//    

namespace Leccion12_Ejercicio02
{
    class Leccion12_Ejercicio02
    {
        // Ahora lo que hace es leer el array y meter en dos listas los strings que 
        // contienen Clone  y los que no, usando  Contains()
        // Ejercicio: Similar, meter en dos listas los de 1 y los de 2 usando IndexOf()

        static string[] nombresEnemigos =          //variables estáticas solo 1 por aplicación (Variable GLOBAL)
       {
                "Lata 1",           // Objetos de la escena precargados
                "Lata 2 (Clone)", // Objetos de la escena instanciados por código, osea, dinámicos
                "Lata 23 (Clone)",
                "Lata 32 (Clone)",
                "Lata 2",
                "Basura 11",
                "Basura 2"
            };
        static void Main(string[] args)
        {
            /*string[] nombresEnemigos =
            {
                "Lata 1",           // Objetos de la escena precargados
                "Lata 2 (Clone)", // Objetos de la escena instanciados por código, osea, dinámicos
                "Lata 23 (Clone)",
                "Lata 32 (Clone)",
                "Lata 2",
                "Basura 11",
                "Basura 2"
            };*/

            string dosPrimeros = "Los dos primeros enemigos son " + nombresEnemigos[0] + ", " + nombresEnemigos[1];
            Console.WriteLine(dosPrimeros);
            List<string> objPre = new List<string>();
            List<string> objClo = new List<string>();

            for (int i = 0; i < nombresEnemigos.Length; i++)
            {
                string enemigoActual = nombresEnemigos[i];
                if (enemigoActual.IndexOf("2") != -1 && enemigoActual.IndexOf(' ', (enemigoActual.IndexOf("2") - 1), 1) != -1)        // if (enemigoActual.IndexOf('2') != -1) VALEN LOS 2
                {                                               //IndexOf(Char, Int32, Int32) POSIBLE
                    objClo.Add(enemigoActual);
                }
                else
                {
                    objPre.Add(enemigoActual);
                }
            }
            Console.WriteLine("Clonados: " + objClo.Count);
            for (int i = 0; i < objClo.Count; i++)
            {
                string enemigoActual = objClo[i];
                Console.WriteLine("Clonado: " + enemigoActual);
            }
            Console.WriteLine("Precargados: " + objPre.Count);
            foreach (string enemAc in objPre)
            {
                Console.WriteLine("Precargado: " + enemAc);
            }


            Console.WriteLine("***NO HA SIDO DE CHIRIPA***");

            Console.WriteLine("Hola que pasa".IndexOf("Nada"));
            Console.WriteLine("Hola que pasa".IndexOf("Hola"));
            Console.WriteLine("Hola que pasa".IndexOf("que"));

        }
    }
}

