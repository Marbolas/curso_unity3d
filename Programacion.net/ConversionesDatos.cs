using System;

public class ConversionesDatos
{
	public static void Main()
	{
		//Convertir número en texto y float en double
		//Conversiones implícitas:
		int edad = 40;
		string resultado;
		resultado = "" + edad;
		resultado = resultado + " era un número y ahora es texto";
		Console.WriteLine(resultado);
		double numDecimal = 6.4545F;
		resultado =  resultado + ", el decimal = " + numDecimal;
		Console.WriteLine(resultado);
		
		//Conversiones explicitas:
		float otroDecimal = (float) 1.23456789123;
		resultado = resultado + ", otroDecimal = " + otroDecimal;
		Console.WriteLine(resultado);
		
		//conversiones complejas de texto a número
		int unEntero = Int32.Parse("3434");
		resultado = resultado + ", un entero = " + unEntero;
		Console.WriteLine(resultado);
		
		string numA = "15", numB = "7";
		//Haz que el programa calcule la suma y muestre el resultado
		
		int unEntero2 = Int32.Parse(numA);
		int unEntero3 = Int32.Parse(numB);
		int sumaEnteros = unEntero2 + unEntero3;
		Console.WriteLine(unEntero2 + " + " + unEntero3 + " = " + sumaEnteros);	

	}
}
