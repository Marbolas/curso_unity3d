﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Leccion11_Ejercicio01
{
    class Leccion11_Ejercicio01     /**Realizar un programa que acumule (sume) valores ingresados por teclado hasta ingresar el 9999 
                                        (no sumar dicho valor, indica que ha finalizado la carga).Imprimir el valor acumulado e informar 
                                        si dicho valor es cero, mayor a cero o menor a cero. */
    {
        static void Main(string[] args)
        {
            int num;
            int suma = 0;
            string linea;
            do
            {
                Console.WriteLine("Introduce un número: ");
                linea = Console.ReadLine();
                num = int.Parse(linea);
                if (num != 9999)
                    suma = suma + num;
            } while (num != 9999);

            Console.WriteLine("El total es:" + suma);
            if (suma < 0)
                Console.WriteLine("la suma es menor que 0");
            else if(suma > 0)
                Console.WriteLine("la suma es mayor que 0");
                else
                Console.WriteLine("la suma es 0");
        }
    }
}
