﻿using System;

namespace Leccion10_Ejercicio01
{
    class Leccion10_Ejercicio01       /**Confeccionar un programa que lea n pares de datos, cada par de datos corresponde a la medida de la base
                                        y la altura
                                        de un triángulo. El programa deberá informar:
                                        a) De cada triángulo la medida de su base, su altura y su superficie. 
                                        b) La cantidad de triángulos cuya superficie es mayor a 12. */
    {
        static void Main(string[] args)
        {
            int contmayores = 0;
           
            float basetriangulo = 0;
            float altura = 0;
            float superficie = 0;
            string linea;

            for (int n = 0; n < 5; n++)
            {
                Console.WriteLine("Introduce base del triangulo: ");
                linea = Console.ReadLine();
                basetriangulo = float.Parse(linea);
                Console.WriteLine("Introduce altura del triangulo: ");
                linea = Console.ReadLine();
                altura = float.Parse(linea);

                superficie = (basetriangulo * altura) / 2;
                if (superficie > 12)
                    contmayores++;

                Console.WriteLine("La base es: " + basetriangulo + " La altura es: " + altura + " La superficie es: " + superficie);
            }
            Console.WriteLine("El numero de triangulos cuya superficie es mayor a 12 son: " + contmayores);
        }
    }
}
