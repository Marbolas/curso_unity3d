﻿using System;

namespace Leccion07_Ejercicio04
{
    class Program   /**Un postulante a un empleo, realiza un test de capacitación, se obtuvo la siguiente información: 
                    cantidad total de preguntas que se le realizaron y la cantidad de preguntas que contestó correctamente.
                    Se pide confeccionar un programa que ingrese los dos datos por teclado e informe el nivel del mismo según el porcentaje
                    de respuestas correctas que ha obtenido, y sabiendo que:
	                Nivel máximo:	Porcentaje>=90%.
	                Nivel medio:	Porcentaje>=75% y <90%.
	                Nivel regular:	Porcentaje>=50% y <75%.
	                Fuera de nivel:	Porcentaje<50%.*/
    {
        static void Main(string[] args)
        {
            float numPreguntas, numRespCorrectas, porcentaje;
            string linea;
            Console.WriteLine("Introducir el número de preguntas:");
            linea = Console.ReadLine();
            numPreguntas = float.Parse(linea);
            Console.WriteLine("Introducir el número de respuestas correctas:");
            linea = Console.ReadLine();
            numRespCorrectas = float.Parse(linea);
            porcentaje = (numRespCorrectas * 100) / numPreguntas;

            if (porcentaje >= 90)
                Console.WriteLine("Tu porcentaje es: " + porcentaje + "% Tienes Nivel máximo Porcentaje >= 90%");
            else
            {
                if (porcentaje >= 75 && porcentaje < 90)
                    Console.WriteLine("Tu porcentaje es: " + porcentaje +"% Tienes Nivel medio Porcentaje >= 75% y < 90%");
                else
                {
                    if (porcentaje >= 50 && porcentaje < 75)
                        Console.WriteLine("Tu posrcentaje es: " + porcentaje + "% Tienes Nivel Regular de Porcentaje >= 50% y < 75%");
                    else
                        Console.WriteLine("Tu porcentaje es: " + porcentaje + "% Fuera de nivel de Porcentaje > 50%");
                }
            }
            Console.ReadKey();
        }
    }
}
