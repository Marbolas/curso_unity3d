﻿using System;

namespace Leccion06_Ejercicio02
{
    class Program   //Se ingresa por teclado un valor entero, mostrar una leyenda que indique si el número es positivo,
                    //nulo o negativo.
    {
        static void Main(string[] args)
        {
            float num;
            string linea;
            Console.WriteLine("Intoduce un número: ");
            linea = Console.ReadLine();
            num = float.Parse(linea);

            if(num > 0)
                Console.WriteLine("El número " + num + " es positivo: ");
                else
                    if(num == 0)
                        Console.WriteLine("El número " + num + " es nulo: ");
                        else
                            Console.WriteLine("El número " + num + " es negativo : ");
            Console.ReadKey();
        }
    }
}
