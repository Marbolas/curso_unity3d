﻿using System;

namespace Leccion09_Ejercicio03
{
    class Leccion09_Ejercicios03          /** En una empresa trabajan n empleados cuyos sueldos oscilan entre $100 y $500, realizar un programa que lea los 
                               sueldos que cobra cada empleado e informe cuántos empleados cobran entre $100 y $300 y cuántos cobran más de $300.
                               Además el programa deberá informar el importe que gasta la empresa en sueldos al personal.*/

    {
        static void Main(string[] args)
        {
            int n = 1;
            float sueldo = 0;
            int numempleados = 1;
            int emp100y300 = 0;
            int empmas300 = 0; ;
            float totalsueldos = 0;
           
            string linea;

            Console.WriteLine("Introduce el número de empleados: ");
            linea = Console.ReadLine();
            numempleados = int.Parse(linea);

            while (n <= numempleados)
            {
                Console.WriteLine("Introduce el sueldo entre $100 y $500 del empleado: " + n);
                linea = Console.ReadLine();
                sueldo = float.Parse(linea);

                if (sueldo < 100 || sueldo > 500)
                    Console.WriteLine("El sueldo no es valido");
                else if (sueldo >= 100 && sueldo <= 300)
                    {
                    totalsueldos = totalsueldos + sueldo;
                    emp100y300++;
                    n++;
                    }
                    else
                    {
                    totalsueldos = totalsueldos + sueldo;
                    empmas300++;
                    n++;
                    }
            }
            Console.WriteLine("El número de personas con un sueldo entre $100 y $300 es: " + emp100y300);
            Console.WriteLine("El número de personas con un sueldo mayor de  $300 es: " + empmas300);
            Console.ReadKey();
        }
    }
}
