﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo_Clases
{
    class Progresion
    {
        public int x;
        public int y;

        public void CargarDatos()
        {
            Console.WriteLine("\n Introduzca el número: ");
            x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("\n Introduzca las veces que se va a repetir:");
            y = Int32.Parse(Console.ReadLine());
        }

        public void GenerarSerie()
        {
            Console.WriteLine("\n " + y + " veces " + x);
            for (int contador = 1; contador <= y; contador++)
                Console.Write(contador * x + " - ");
            Console.WriteLine("\n");
        }

        public void MostrarProgresion2(int x, int y)
        {
            Console.WriteLine("\n " + y + " veces " + x);
            for (int contador = 1; contador <= y; contador++)
                Console.Write(contador * x + " - ");
            Console.WriteLine("\n");
        }
    }
}
