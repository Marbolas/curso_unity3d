﻿using System;

namespace Ejemplo_Clases
{
    class Program
    {
        static void Main()
        {
            Progresion nuevaSerie = new Progresion();
                
            nuevaSerie.CargarDatos();
            nuevaSerie.GenerarSerie();
            nuevaSerie.MostrarProgresion2(25, 11);

            nuevaSerie.x = 7;
            nuevaSerie.y = 3;
            nuevaSerie.GenerarSerie();

            nuevaSerie.x = 13;
            nuevaSerie.y = 9;
            nuevaSerie.GenerarSerie();

            Progresion secuenciasPares = new Progresion();
            secuenciasPares.x = 2;
            secuenciasPares.y = 5;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 4;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 6;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 8;
            secuenciasPares.GenerarSerie();
        }





        // Este va 25 veces, de 11 en 11 a partir del 11.
        static void MainPorFunciones(string[] args)
        {
            //Console.WriteLine("25 veces 11");
            // for (int contador = 1; contador <= 25; contador++)
            //    Console.Write(contador * 11 + " - ");
            MostrarProgresion(11, 25);

            //Console.WriteLine("\n7 veces 3");
            //for (int contador = 1; contador <= 7; contador++)
            //    Console.Write(contador * 3 + " - ");
            MostrarProgresion(3, 7);

            //Console.WriteLine("\n13 veces 9");
            //for (int contador = 1; contador <= 13; contador++)
            //    Console.Write(contador * 9 + " - ");
            MostrarProgresion(9, 13);

            Console.WriteLine("\nIntroduzca veces y repetición:");
            int x = int.Parse(Console.ReadLine());
            int y = int.Parse(Console.ReadLine());
            //for (int contador = 1; contador <= x; contador++)
            //Console.Write(contador * y + " - ");
            MostrarProgresion(x, y);
        }
        // Ahora que vaya X veces, de Y en Y, a partir de Y. 
        // Donde X, e Y, pueden ser muchos tipos. Por ejemplo. 
        // Haz para el X= 7, Y = 3.
        //  X= 13, Y = 9
        // Para X= lo que el usuario meta, e Y también
        // Imaginaros otros 20 casos

        // Mejora 1: Programación funcional: Con una función, en C#, una función "pura" es un MÉTODO ESTÁTICO
        public static void MostrarProgresion(int x, int y)
        {
            Console.WriteLine("\n " + y + " veces " + x);
            for (int contador = 1; contador <= y; contador++)
                Console.Write(contador * x + " - ");
        }
    }

}
