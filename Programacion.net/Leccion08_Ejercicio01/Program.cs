﻿using System;

namespace Leccion08_Ejercicio01
{
    class Program       /**Realizar un programa que pida cargar una fecha cualquiera, luego verificar si dicha fecha
                           corresponde a Navidad.*/
    {
        static void Main(string[] args)
        {
            int dia;
            string mes;
            string linea;

            Console.WriteLine("Introduce el día:" );
            linea = Console.ReadLine();
            dia = int.Parse(linea);
            Console.WriteLine("Introduce el mes:");
            linea = Console.ReadLine();
            mes = (linea);

            if (dia == 25 && mes == "Diciembre")
                Console.WriteLine("El día " + dia + " de " + mes + " es Navidad");
                else
                Console.WriteLine("El día " + dia + " de " + mes + " no es Navidad");
        }
    }
}
